#include "Engine.hpp"
#include <SFML/Window/Event.hpp>
#include <boost/any.hpp>

#include "Console.hpp"
#include "RandomGenerator.hpp"

void CMD_CLEAR_F(const Command*, const ArgList&)
{
    CONSOLE.clearHistory();
}

void CMD_HELP_F(const Command*, const ArgList&)
{
    LOG(grey, "If you need help with a particular command, type the command followed by \"?\", eg \"cmdlist ?\".");
    
    LOG(grey, "\nCommand helpers are formatted as follows:");
    LOG(purple, "   Command description.");
    LOG(white, "   syntax: cmd <required> [optional] ...");

    LOG(grey, "\nUse \"cmdlist\" for a list of commands.");
    LOG(grey, "Use \"cvarlist\" for a list of cvars.");
}

// CMD_QUIT_F
// Safely terminates the game by ending
// the main game loop.
void Engine::CMD_QUIT_F(const Command*, const ArgList&)
{
    eventManager.sendEvent(EVENT_EXIT);
    m_running = false;
}

void Engine::CMD_LAUNCH_F(const Command*, const ArgList&) {
    sendEvent(EVENT_CMD_LAUNCH);
}

void Engine::EV_HOSTED_GAME_F() {
    state.state = GAMESTATE_LOBBY;
}
void Engine::EV_CONNECTED_TO_GAME_F() {
    state.state = GAMESTATE_LOBBY;
}
void Engine::EV_DISCONNECTED_FROM_GAME_F() {
    state.state = GAMESTATE_MENU;
}
void Engine::EV_LAUNCH_F() {
    state.state = GAMESTATE_INGAME;
    state.currentTick = 0;
    world.createTestMap(60, 30);
    pathfinder.initNewTileMap();
    world.createUnit(sf::Vector2i(200, 200));
}
void Engine::EV_NEXT_TICK_F(const Event&) {
    update();
}
void Engine::EV_SELECTION_BOX_F(const Event& e) {
    sf::IntRect bounds = boost::any_cast<sf::IntRect>(e.data[0]);
    for (auto& unit : world.m_unitList) {
        unit.selected = false;
        if (bounds.contains(sf::Vector2i(unit.getPosition()))) {
            unit.selected = true;
        }
    }
}

Engine::Engine() :
    renderWindow(sf::VideoMode(960, 540), "Formations", sf::Style::Close,
                 sf::ContextSettings(0, 0, 0, 1, 1, 0))
{
    // Inject dependencies for systems:
    commandSystem.inject (&state, &cvarSystem);
    cvarSystem.inject    (&commandSystem);
    fileSystem.inject    (&cvarSystem);
    networkSystem.inject (&state, &world, &random, &cvarSystem, &commandSystem);
    inputSystem.inject   (&commandSystem);
    renderSystem.inject  (&renderWindow, &state, &world, &cvarSystem, &commandSystem);
    pathfinder.inject(&world);

    initEventManager(&eventManager);
    cvarSystem.initEventManager(&eventManager);
    fileSystem.initEventManager(&eventManager);
    networkSystem.initEventManager(&eventManager);
    inputSystem.initEventManager(&eventManager);
    renderSystem.initEventManager(&eventManager);
    pathfinder.initEventManager(&eventManager);

    init();
    commandSystem.init();
    cvarSystem.init();
    fileSystem.init();
    networkSystem.init();
    inputSystem.init();
    renderSystem.init();
    pathfinder.init();

    world.createEmptyMap(10, 10);
    pathfinder.initNewTileMap();

    renderWindow.setKeyRepeatEnabled(false);
        
    // Register commands
    commandSystem.registerCmd("help", CMD_HELP_F, CMD_FLAG_NONE, "Basic help text.", "help");
    commandSystem.registerCmd("clear", CMD_CLEAR_F, CMD_FLAG_NONE, "Clear the console history.", "clear");
    commandSystem.registerCmd("quit", std::bind(&Engine::CMD_QUIT_F, this, ph::_1, ph::_2), CMD_FLAG_NONE, "Immediately quit the game.", "quit");
    commandSystem.registerCmd("launch", std::bind(&Engine::CMD_LAUNCH_F, this, ph::_1, ph::_2), CMD_FLAG_NONE, "Launch the game.", "launch");


    // Register CVARs
    cvarSystem.registerCvar(Cvar("cl_reportdebug", true, CVAR_FLAG_NONE, "Report debug level logging in console."));
    
    CONSOLE.setVisibility(true);

    fileSystem.readCvarsFromFile();
    cvarSystem.initCvars();

    run();
}

void Engine::init()
{
    std::vector<EventType> es {
        EVENT_HOSTED_GAME,
            EVENT_CONNECTED_TO_GAME,
            EVENT_DISCONNECTED_FROM_GAME,
            EVENT_SERVER_DISCONNECTED,
            EVENT_LAUNCH,
            EVENT_READY_TO_SIM_NEXT_TICK,
            EVENT_SELECTION_BOX
    };
    subscribe(es);
}

void Engine::onEvent(const Event& e)
{
    switch (e.type)
    {
    case EVENT_HOSTED_GAME: EV_HOSTED_GAME_F(); break;
    case EVENT_CONNECTED_TO_GAME: EV_CONNECTED_TO_GAME_F(); break;
    case EVENT_SERVER_DISCONNECTED: // fallthrough
    case EVENT_DISCONNECTED_FROM_GAME: EV_DISCONNECTED_FROM_GAME_F(); break;
    case EVENT_LAUNCH: EV_LAUNCH_F(); break;
    case EVENT_READY_TO_SIM_NEXT_TICK: EV_NEXT_TICK_F(e); break;
    case EVENT_SELECTION_BOX: EV_SELECTION_BOX_F(e); break;
    default: break;
    }
}

void Engine::run()
{
    sf::Clock frameTimer;

    m_currentTime = sf::Time::Zero;
    sf::Time renderInterval {sf::Time::Zero};       // as fast as possible
    
    // Game loop
    m_running = true;
    while (m_running)
    {        
        renderInterval = frameTimer.restart();
        m_currentTime += renderInterval;
        
        handleInput();
        networkSystem.update(m_currentTime, renderInterval);
        pathfinder.update();
                
        // Render as fast as possible
        renderSystem.update(m_currentTime, renderInterval);
    }

    // Exit game safely when loop is finished
    return;
}

void Engine::update()
{
    state.currentTickBeginTime = m_currentTime;
    state.currentTick++;
}

void Engine::deinit()
{
    m_running = false;
}

void Engine::handleInput()
{
    sf::Event e;
    while (renderWindow.pollEvent(e))
    {               
        inputSystem.handleInput(e);
    };
}
