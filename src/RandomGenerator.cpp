#include "RandomGenerator.hpp"
#include "Console.hpp"

#include <random>
#include <chrono>

#define ARRAY_SIZE(a)                               \
  ((sizeof(a) / sizeof(*(a))) /                     \
  static_cast<size_t>(!(sizeof(a) % sizeof(*(a)))))

RandomGenerator::RandomGenerator()
{
    seed();
}

RandomGenerator::result_type RandomGenerator::generate()
{
    return engine();
}

RandomGenerator::result_type RandomGenerator::generate(int min, int max)
{
    std::uniform_int_distribution<> dist{min, max};
    return dist(engine);
}

RandomGenerator::result_type RandomGenerator::getSeed()
{
    return engine.seed;
}

void RandomGenerator::seed()
{
    seed((RandomGenerator::result_type)std::chrono::high_resolution_clock::now().time_since_epoch().count());
}
void RandomGenerator::seed(uint32_t n)
{
    engine.index = 0;
    // Expand the seed with the same algorithm as boost::random::mersenne_twister
    const uint32_t mask = ~0u;
    engine.state[0] = n & mask;
    for (uint32_t i = 1; i < ARRAY_SIZE(engine.state); ++i) {
        engine.state[i] = (1812433253UL * (engine.state[i - 1] ^ (engine.state[i - 1] >> 30)) + i) & mask;
    }

    LOG(debug, "Reseeded RNG with seed %u.", n);
}
