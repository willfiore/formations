#include "Cvar.hpp"
#include "Console.hpp"
#include "CvarSystem.hpp"

Cvar::Cvar(std::string _name, std::string _defaultValue, int _maxchars, int _flags, std::string _description) :
    name(_name),
    description(_description),
    type(CvarType::String),
    maxValue(float(_maxchars)),
    defaultValue(_defaultValue),
    flags(_flags)
{
    value = _defaultValue;
}

Cvar::Cvar(std::string _name, int _defaultValue, float _min, float _max, int _flags, std::string _description) :
    name(_name),
    description(_description),
    type(CvarType::Int),
    minValue(_min),
    maxValue(_max),
    defaultValue(std::to_string(_defaultValue)),
    flags(_flags)
{
    value = std::to_string(_defaultValue);
}

Cvar::Cvar(std::string _name, float _defaultValue, float _min, float _max, int _flags, std::string _description) :
    name(_name),
    description(_description),
    type(CvarType::Float),
    minValue(_min),
    maxValue(_max),
    defaultValue(std::to_string(_defaultValue)),
    flags(_flags)
{
    value = std::to_string(_defaultValue);
}

Cvar::Cvar(std::string _name, bool _defaultValue, int _flags, std::string _description) :
    name(_name),
    description(_description),
    type(CvarType::Bool),
    minValue(0),
    maxValue(1),
    defaultValue(std::to_string(_defaultValue ? 1 : 0)),
    flags(_flags)
{
    value = std::to_string(_defaultValue);
}

bool Cvar::isSettable() const
{
    if (flags & CVAR_FLAG_READONLY) {
        return false;
    }
    return true;
}

void Cvar::setValue(std::string v, bool silent)
{
    // Sanity check value
    if (type != CvarType::String)
    {
        float cvalue;
        try { cvalue = std::stof(v); }
        catch (const std::exception&) {
            LOG(warn, "Tried to set invalid value to cvar %s.", name.c_str());
            return;
        }
        if (type == CvarType::Bool) {
            setValue_Bool(cvalue > 0, silent);
        }
        else if (type == CvarType::Float) {
            setValue_Float(cvalue, silent);
        }
        else if (type == CvarType::Int) {
            setValue_Int(int(cvalue), silent);
        }
    }
    else {
        setValue_String(v, silent);
    }    
}

void Cvar::setValue_String(std::string v, bool silent)
{
    if(v.length() > (unsigned int)maxValue) v = v.substr(0, (unsigned int)maxValue);
    value = v;
    if (!silent)      cvarSystem->notifyChange(this);
}
void Cvar::setValue_Int(int v, bool silent)
{
    if (v < minValue) value = std::to_string(minValue);
    else if (v > maxValue) value = std::to_string(maxValue);
    else value = std::to_string(v);
    if (!silent)      cvarSystem->notifyChange(this);
}
void Cvar::setValue_Float(float v, bool silent)
{
    if (v < minValue) value = std::to_string(minValue);
    else if (v > maxValue) value = std::to_string(maxValue);
    else value = std::to_string(v);
    if (!silent)      cvarSystem->notifyChange(this);
}
void Cvar::setValue_Bool(bool v, bool silent)
{
    value = std::to_string(v);
    if (!silent)      cvarSystem->notifyChange(this);
}

void Cvar::logInfoString() const
{
    switch (type) {
    case CvarType::Bool:
        LOG(white, "'%s' = %u", getName().c_str(), getValue_Bool());
        LOGC(grey, " (default: %s, boolean)", getValue_String_Default().c_str());
        break;
    case CvarType::Int:
        LOG(white, "'%s' = %i", getName().c_str(), getValue_Int());
        LOGC(grey, " (default: %s, range: %.0f to %.0f)", getValue_String_Default().c_str(), getMinValue(), getMaxValue());
        break;
    case CvarType::Float:
        LOG(white, "'%s' = %.2f", getName().c_str(), getValue_Float());
        LOGC(grey, " (default: %.2f, range: %.2f to %.2f)", std::stof(getValue_String_Default().c_str()), getMinValue(), getMaxValue());
        break;
    case CvarType::String:
        LOG(white, "'%s' = \"%s\"", getName().c_str(), getValue_String().c_str());
        LOGC(grey, " (default: \"%s\")", getValue_String_Default().c_str());
        break;
    default:
        break;
    }

    if (flags & CVAR_FLAG_CHEAT)
        LOGC(red, " cheat");
    if (flags & CVAR_FLAG_READONLY)
        LOGC(red, " read-only");
}
