#include "Console.hpp"
#include "CommandSystem.hpp"

#include <cstdarg>
#include <cstdio>

Console CONSOLE;

std::deque<ConsoleItem>* Console::getHistory()
{
    return &history;
}

void Console::clearHistory()
{
    history.clear();
}

void Console::log(ConsoleItemType _type, bool _continuous, const char* fmt, ...)
{
    ConsoleItem item;
    item.type = _type;
    item.continuous = _continuous;
    
    char buffer[256];
    va_list args;
    va_start(args, fmt);
    vsprintf(buffer, fmt, args);
    va_end(args);
    
    item.msg = buffer;

    addToHistory(item);
}

void Console::addToHistory(ConsoleItem item)
{
    history.push_back(item);

    // Limit size
    if (history.size() > maxSize)
    {
        history.pop_front();
    }
}

void Console::setVisibility(int visibility)
{
    if (visibility > 0)       visible = true;
    else if (visibility == 0) visible = false;
    else if (visibility < 0)  visible = !visible;

    if (visible)
    {
        scrollToBottom = true;
        focusOnInput = true;
    }
}

bool Console::isVisible()
{
    return visible;
}
