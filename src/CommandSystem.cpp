﻿#include "CommandSystem.hpp"
#include "CvarSystem.hpp"

#include "GameState.hpp"

#include <sstream>
#include <ctype.h>

CommandSystem::CommandSystem()
{
}

void CommandSystem::init()
{
    registerCmd("cmdlist", std::bind(&CommandSystem::CMD_CMDLIST_F, this, ph::_1, ph::_2), CMD_FLAG_NONE, "List commands.", "cmdlist [filter]");
}

void CommandSystem::registerCmd(const std::string& name, const std::function<void(const Command* cmd, const ArgList& args)>& func, int flags, const std::string& desc, const std::string& usage)
{
    Command c(name, func, flags, desc, usage);
    cmdList.insert(std::make_pair(name, c));
}

void CommandSystem::executeCmdString(std::string cmd)
{
    if (cmd.empty()) return;
    
    // remove preceding whitespace
    while (isspace(cmd[0])) {
        cmd.erase(0, 1);
    }

    // Remove a single forward slash, backslash
    if (cmd[0] == '/' || cmd[0] == '\\') {
        cmd.erase(0, 1);
    }

    std::string name;
    std::vector<std::string> arg_tokens;
    std::string item;

    // Add trailing space to capture final argument
    cmd.append(" ");
    
    int s = 0;
    bool q = false;
    for (size_t i = s; i < cmd.length(); ++i) {
        char c = cmd[i];
        if (c == '\"') {
            q = !q;
            cmd.erase(cmd.begin()+i);
            i--;
        }
        if (i == cmd.length() - 1) q = false;
        if (c == ' ') {
            if (q) continue;
            std::string arg = cmd.substr(s, i-s);
            if (!arg.empty()) {
                if (name.empty()) name = cmd.substr(s, i-s);
                else arg_tokens.push_back(cmd.substr(s, i-s));
            }
            s = i+1;
        }
    }

    // Convert cmd name to lowercase before checking
    std::transform(name.begin(), name.end(), name.begin(), ::tolower);

    const ArgList args(arg_tokens);

    if (exists(name))
    {
        // Display help if first parameter is "?"
        if (args.count() > 0) {
            if (args.at(0) == "?") {
                logHelpInfo(&cmdList.at(name));
                return;
            }
        }

        cmdList.at(name).execute(args);
    }
    else if (cvarSystem->exists(name))
    {
        if (args.count() == 0) {
            cmdList.at("get").execute(ArgList(name));
        }
        else {
            if (args.at(0) == "?") {
                cmdList.at("get").execute(ArgList(name));
            } else {
                ArgList setArgs(name);
                setArgs.append(arg_tokens);
                cmdList.at("set").execute(setArgs);
            }
        }
    }
    else {
        LOG(red, "Unknown cmd '%s'.", name.c_str());
    }
}

void CommandSystem::execute(const Command* cmd, const ArgList& args)
{
    if(cmd->getFlags() & CMD_FLAG_CONNECTED) {
        if (gameState->state & (GAMESTATE_NONE | GAMESTATE_MENU)) {
            LOG(red, "You must be ingame to do that.");
            return;
        }
    }
    
    cmd->execute(args);
}

// CMD_CMDLIST_F
// List commands in console. Allows filtering.
void CommandSystem::CMD_CMDLIST_F(const Command*, const ArgList& args)
{
    if (args.count() == 0)
    {
        LOG(grey, "List of commands (no filter):");

        for (auto i : cmdList)
        {
            LOG(white, "%-20s", i.first.c_str());
            LOGC(purple, "* ");
            LOGC(purple, "%s", i.second.getDescription().c_str());
        }
    }
    else {
        LOG(grey, "List of commands (filter ");
        LOGC(green, "%s", args.at(0).c_str());
        LOGC(grey, "):");

        for (auto i : cmdList)
        {
            const size_t firstFound = i.first.find(args.at(0));
            if (firstFound != std::string::npos)
            {
                LOG(white, "%s", i.first.substr(0, firstFound).c_str());
                LOGC(green, "%s", args.at(0).c_str());
                LOGC(white, "%-*s", 20 - firstFound - args.at(0).length(), i.first.substr(firstFound + args.at(0).length(), std::string::npos).c_str());
                LOGC(purple, "* ");
                LOGC(purple, "%s", i.second.getDescription().c_str());
            }
        }
    }
}

void CommandSystem::logHelpInfo(const Command* cmd)
{
    LOG(purple, cmd->getDescription().c_str());
    LOG(white, "syntax: %s", cmd->getUsage().c_str());
}
