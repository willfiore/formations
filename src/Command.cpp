#include "Command.hpp"

Command::Command(const std::string& _name,
                 const std::function<void(const Command*, const ArgList&)> _func,
                 int _flags, const std::string& _description, const std::string& _usage) :
    name(_name),
    description(_description),
    usage(_usage),
    flags(_flags),
    func(_func)
{
}

void Command::execute(const ArgList& args) const
{
    func(this, args);
}
