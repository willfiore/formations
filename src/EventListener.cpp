#include "EventListener.hpp"

#include "EventManager.hpp"
#include "Event.hpp"
#include "Console.hpp"

EventListener::EventListener()
{
};

void EventListener::initEventManager(EventManager* _eventManager)
{
    el_eventManager = _eventManager;
}

void EventListener::subscribe(const std::vector<EventType>& v)
{
    for (auto i : v)
    {
        subscribe(i);
    }
}

void EventListener::subscribe(EventType e)
{
    el_eventManager->addSubscriber(e, this);
}

void EventListener::unsubscribe(EventType e)
{
    el_eventManager->removeSubscriber(e, this);
}

void EventListener::sendEvent(const Event& e)
{
    el_eventManager->sendEvent(e);
}
void EventListener::sendEvent(EventType t)
{
    el_eventManager->sendEvent(t);
}
