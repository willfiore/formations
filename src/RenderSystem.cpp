#include "RenderSystem.hpp"

#include "CvarSystem.hpp"
#include "CommandSystem.hpp"
#include "GameState.hpp"
#include "World.hpp"

#include <SFML/Window/Mouse.hpp>
#include <SFML/Graphics/Text.hpp>
#include <SFML/OpenGL.hpp>

#include <imgui.h>
#include <imgui-SFML.h>

#include <algorithm>

void RenderSystem::CMD_SELECT_F(const Command*, const ArgList&, bool release) {
    if (!release) {
        m_isDrawingSelectionBox = true;
        m_selectionBox.left = sf::Mouse::getPosition(*renderWindow).x;
        m_selectionBox.top = sf::Mouse::getPosition(*renderWindow).y;
    } else {
        m_isDrawingSelectionBox = false;
        Event e(EVENT_SELECTION_BOX);
        e.data.push_back(m_selectionBox);
        sendEvent(e);
    }
}

void RenderSystem::EV_CVAR_CHANGED_F(const Event& e)
{
    Cvar* cvar = boost::any_cast<Cvar*>(e.data[0]);
    std::string name = cvar->getName();
    
    if (name == "r_vsync") {
        renderWindow->setVerticalSyncEnabled(cvar->getValue_Bool());
    }
    else if (name == "r_maxfps") {
        renderWindow->setFramerateLimit(cvar->getValue_Int());
    }
    else if (name == "r_showfps") {
        m_drawFps = cvar->getValue_Bool();
    }
}

void RenderSystem::EV_WINDOW_RESIZED_F(const Event& e)
{
    unsigned int width  = boost::any_cast<unsigned int>(e.data[0]);
    unsigned int height = boost::any_cast<unsigned int>(e.data[1]);
    glViewport(0, 0, width, height);
}

RenderSystem::RenderSystem() :
    m_isDrawingSelectionBox(false)
{
}

void RenderSystem::init()
{
    // Cvars
    cvarSystem->registerCvar(Cvar("r_vsync", true, CVAR_FLAG_NONE, "Enable/disable vertical sync."));
    cvarSystem->registerCvar(Cvar("r_maxfps", 0, 0, 300, CVAR_FLAG_NONE, "Roughly limit framerate to value. 0 to disable."));
    cvarSystem->registerCvar(Cvar("r_showfps", false, CVAR_FLAG_NONE, "Draw current frames per second value."));

    // CMDs
    commandSystem->registerCmd("+select", std::bind(&RenderSystem::CMD_SELECT_F, this, ph::_1, ph::_2, false), CMD_FLAG_NONE, "Begin drawing a selection box.", "+select");
    commandSystem->registerCmd("-select", std::bind(&RenderSystem::CMD_SELECT_F, this, ph::_1, ph::_2, true), CMD_FLAG_NONE, "Finish drawing a selection box.", "-select");
        
    ImGui::SFML::Init(*renderWindow);
    ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 2.f);
    ImGui::PushStyleColor(ImGuiCol_WindowBg, ImColor(50, 50, 50, 200));

    m_fpsFont.loadFromFile("../assets/fonts/inconsolata.ttf");

    subscribe(EVENT_CVAR_CHANGED);
    subscribe(EVENT_WINDOW_RESIZED);
}

void RenderSystem::onEvent(const Event& e)
{
    switch (e.type)
    {
    case EVENT_CVAR_CHANGED: EV_CVAR_CHANGED_F(e); break;
    case EVENT_WINDOW_RESIZED: EV_WINDOW_RESIZED_F(e); break;
    default:
        break;
    }
}

void RenderSystem::update(const sf::Time& currentTime, const sf::Time& delta)
{
    // Calculate LERP
    m_lerpValue = float(currentTime.asMilliseconds() - gameState->currentTickBeginTime.asMilliseconds()) / gameState->tickDelta.asMilliseconds();
    if (m_lerpValue > 1) m_lerpValue = 1;
    
    glClearColor(0.0, 0.0, 0.0, 0.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    drawTiles();
    drawUnits();
    drawSelectionBox();

    ImGui::SFML::Update(delta);
    drawDebug();

    ImGui::Render();
    renderWindow->display();
}

void RenderSystem::drawTiles()
{
    const TwoVector<Tile>& tileMap = world->getTileMap();
    
    unsigned int numTiles = tileMap.getWidth() * tileMap.getHeight();
    int tileSize = 16;

    sf::VertexArray renderedTiles;
    renderedTiles.setPrimitiveType(sf::Quads);
    renderedTiles.resize(numTiles * 4);
    
    for (int i = 0; i < tileMap.getWidth(); ++i) {
        for (int j = 0; j < tileMap.getHeight(); ++j) {
            sf::Vertex* quad = &renderedTiles[(i + j*tileMap.getWidth())*4];
            quad[0].position = sf::Vector2f(i*tileSize, j*tileSize);
            quad[1].position = sf::Vector2f((i+1)*tileSize, j*tileSize);
            quad[2].position = sf::Vector2f((i+1)*tileSize, (j+1)*tileSize);
            quad[3].position = sf::Vector2f(i*tileSize, (j+1)*tileSize);

            sf::Color tileColor = sf::Color(0, 20, 50, 255);
            if (!tileMap.at(i, j).isWalkable) {
                tileColor += sf::Color(205, 0, 0, 0);
            }
            
            tileColor += sf::Color(std::min<int>(tileMap.at(i, j).debug_integrationCost*10, 255), 0, 0, 0);
            
            switch(tileMap.at(i, j).debug_Color) {
            case 0: break;
                // Portal edge
            case 1: tileColor = sf::Color(100, 100, 0, 255); break;
                // Portal center
            case 2: tileColor = sf::Color(100, 150, 0, 255); break;
                // Goal
            case 3: tileColor = sf::Color(0, 200, 200, 255); break;
            }
            quad[0].color = tileColor;
            quad[1].color = tileColor;
            quad[2].color = tileColor - sf::Color(50, 50, 50, 0);
            quad[3].color = tileColor - sf::Color(30, 30, 30, 0);
        }
    }
    renderWindow->draw(renderedTiles);
}

void RenderSystem::drawUnits()
{
    // Draw units
    sf::RectangleShape unitShape;
    unitShape.setSize(sf::Vector2f(8, 8));
    unitShape.setOrigin(2, 2);
    unitShape.setRotation(45);

    const std::vector<Unit> unitList = world->getUnitList();
    for (auto& unit : unitList) {
        unitShape.setPosition(sf::Vector2f(sf::Vector2i(sf::Vector2f(unit.getOldPosition()) + m_lerpValue*sf::Vector2f(unit.getDeltaPosition()))));
        if (unit.selected) {
            unitShape.setFillColor(sf::Color::Green);
        } else {
            unitShape.setFillColor(sf::Color::White);
        }
        renderWindow->draw(unitShape);
    }
}

void RenderSystem::drawSelectionBox()
{
    if (m_isDrawingSelectionBox) {
        m_selectionBox.width = sf::Mouse::getPosition(*renderWindow).x - m_selectionBox.left;
        m_selectionBox.height = sf::Mouse::getPosition(*renderWindow).y - m_selectionBox.top;
        sf::RectangleShape boxShape;
        boxShape.setFillColor(sf::Color(255, 255, 255, 100));
        boxShape.setOutlineColor(sf::Color(255, 255, 255, 180));
        boxShape.setOutlineThickness(1);
        boxShape.setPosition(m_selectionBox.left, m_selectionBox.top);
        boxShape.setSize(sf::Vector2f(m_selectionBox.width, m_selectionBox.height));
        renderWindow->draw(boxShape);
    }
}

void RenderSystem::drawDebug()
{
    // Debug UI
    if (CONSOLE.isVisible()) {
        drawConsole();
    }

    if (m_drawFps) {
        std::string fpsString;
        fpsString = std::to_string(int(ImGui::GetIO().Framerate));
        sf::Text fpsText;
        fpsText.setPosition(10, 5);
        fpsText.setFont(m_fpsFont);
        fpsText.setCharacterSize(24);
        fpsText.setFillColor(sf::Color::Yellow);
        fpsText.setString(fpsString);
        renderWindow->draw(fpsText);
    }
}

void RenderSystem::drawConsole()
{
    ImGui::Begin("Debug Console");

    if (ImGui::GetWindowSize().x < 600) ImGui::SetWindowSize(ImVec2(600, ImGui::GetWindowSize().y));
    if (ImGui::GetWindowSize().y < 400) ImGui::SetWindowSize(ImVec2(ImGui::GetWindowSize().x, 400));
    ImGui::Text("Tick: %u (+%u buffered)", gameState->currentTick, gameState->ticksAhead);
    ImGui::Separator();
    ImGui::BeginChild("ConsoleRegion", ImVec2(0, -ImGui::GetItemsLineHeightWithSpacing()), false);
    ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(0.5f, 1));

    for (unsigned int i = 0; i < CONSOLE.getHistory()->size(); ++i)
    {
        ConsoleItem element = CONSOLE.getHistory()->at(i);

        if(element.type == ConsoleItemType::debug && !cvarSystem->getCvar("cl_reportdebug")->getValue_Bool()) continue;

        std::string prepend = "";
        ImColor color = ImColor(255, 255, 255, 255);

        switch (element.type)
        {
        case ConsoleItemType::info:
            prepend = "";
            color = ImColor(70, 230, 255, 255);
            break;
        case ConsoleItemType::debug:
            prepend = "[debug] ";
            color = ImColor(235, 205, 60, 255);
            break;
        case ConsoleItemType::warn:
            prepend = "";
            color = ImColor(235, 65, 65, 255);
            break;
        case ConsoleItemType::input:
            prepend = "> ";
            color = ImColor(235, 235, 30, 255);
            break;
        case ConsoleItemType::white:
            color = ImColor(230, 230, 230, 255);
            break;
        case ConsoleItemType::purple:
            color = ImColor(200, 130, 225, 255);
            break;
        case ConsoleItemType::red:
            color = ImColor(235, 65, 65, 255);
            break;
        case ConsoleItemType::green:
            color = ImColor(70, 230, 100, 255);
            break;
        case ConsoleItemType::cyan:
            color = ImColor(70, 230, 255, 255);
            break;
        case ConsoleItemType::grey:
            color = ImColor(168, 168, 168, 255);
            break;
        default:
            break;
        }

        ImGui::PushStyleColor(ImGuiCol_Text, color);

        if (element.continuous) ImGui::SameLine();

        ImGui::Text("%s", prepend.c_str()); ImGui::SameLine();
        ImGui::TextWrapped("%s", element.msg.c_str());
        ImGui::PopStyleColor();
    }

    ImGui::PushStyleColor(ImGuiCol_Text, ImColor(235, 235, 30, 255));
    ImGui::Text(">");
    ImGui::PopStyleColor();

    if (CONSOLE.scrollToBottom)
    {
        ImGui::SetScrollPosHere();
        CONSOLE.scrollToBottom = false;
    }

    // Sticky bottom
    if (ImGui::GetScrollY() >= (ImGui::GetScrollMaxY() - 10)) {
        ImGui::SetScrollPosHere();
    }

    ImGui::PopStyleVar();
    ImGui::EndChild();
    ImGui::PushItemWidth(-1);
    if (ImGui::InputText("", CONSOLE.inputText, 80, ImGuiInputTextFlags_EnterReturnsTrue)) 
    {
        for (int i = 0; i < 256; ++i) {
            if (!isspace(CONSOLE.inputText[i]) && CONSOLE.inputText[i] != '\0') {
                LOG(input, CONSOLE.inputText);
                commandSystem->executeCmdString(CONSOLE.inputText);
                CONSOLE.scrollToBottom = true;
                break;
            }
        }

        CONSOLE.inputText[0] = '\0';
        CONSOLE.focusOnInput = true;
    }

    if (CONSOLE.focusOnInput)
    {
        ImGui::SetKeyboardFocusHere();
        CONSOLE.focusOnInput = false;
    }
    ImGui::PopItemWidth();
    ImGui::End();
}
