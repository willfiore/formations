#include "EventManager.hpp"
#include "EventListener.hpp"

EventManager::EventManager()
{
}

void EventManager::addSubscriber(EventType type, EventListener* listener)
{
    if (!subscriberList.count(type))
    {
        auto inserted = subscriberList.insert(std::make_pair(type, std::vector<EventListener*>()));
        inserted.first->second.push_back(listener);
    }
    else
    {
        auto iter = std::find(subscriberList[type].begin(), subscriberList[type].end(), listener);
        if (iter == subscriberList[type].end())
        {
            subscriberList[type].push_back(listener);
        }
    }
}

void EventManager::removeSubscriber(EventType type, EventListener* listener)
{
    if (subscriberList.count(type))
    {
        auto iter = std::find(subscriberList[type].begin(), subscriberList[type].end(), listener);
        if (iter != subscriberList[type].end())
        {
            subscriberList[type].erase(iter);
        }
    }
}

void EventManager::sendEvent(EventType type)
{
    Event e(type);
    sendEvent(e);
}

void EventManager::sendEvent(Event e)
{
    if (subscriberList.count(e.type))
    {
        for (auto l : subscriberList[e.type])
        {
            l->onEvent(e);
        }
    }
}
