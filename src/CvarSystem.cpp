#include "CvarSystem.hpp"
#include "CommandSystem.hpp"
#include "Console.hpp"

// CMD_SET_F
// Set the value of a cvar
void CvarSystem::CMD_SET_F(const Command* cmd, const ArgList& args)
{
    if (args.count() < 2) {
        commandSystem->logHelpInfo(cmd);
        return;
    }

    if (!exists(args.at(0))) {
        LOG(red, "Unknown cvar %s.", args.at(0).c_str());
        return;
    }

    Cvar* cvar = getCvar(args.at(0));

    // Is the user allowed to set it?
    if (!cvar->isSettable()) {
        LOG(red, "Not allowed to change read-only cvar %s.", cvar->getName().c_str());
        return;
    }

    // (Try to) change the value
    cvar->setValue(args.at(1));
    
    //cvar->logInfoString();
}

// CMD_GET_F
// Get the value of a cvar
void CvarSystem::CMD_GET_F(const Command* cmd, const ArgList& args)
{
    if (args.count() < 1) {
        commandSystem->logHelpInfo(cmd);
        return;
    }

    if (!exists(args.at(0))) {
        LOG(red, "unknown cvar '%s'", args.at(0).c_str());
        return;
    }
    
    Cvar* cvar = getCvar(args.at(0));
      
    LOG(purple, "%s", cvar->getDescription().c_str());

    cvar->logInfoString();
}

/*/////////////////////////////////////////////////////
  CVAR SYSTEM
/////////////////////////////////////////////////////*/

CvarSystem::CvarSystem() :
    nullCvar("NULL_CVAR", 0, 0, 0, CVAR_FLAG_NONE, "")
{
}

void CvarSystem::onEvent(const Event&)
{
}

void CvarSystem::init()
{
    // Command registration
    commandSystem->registerCmd("set", std::bind(&CvarSystem::CMD_SET_F, this, ph::_1, ph::_2), CMD_FLAG_NONE, "Change the value of a cvar.", "[\"set\"] <cvar> <value>");
    commandSystem->registerCmd("get", std::bind(&CvarSystem::CMD_GET_F, this, ph::_1, ph::_2), CMD_FLAG_NONE, "Retrieve the value of a cvar.", "[\"get\"] <cvar>");
    commandSystem->registerCmd("cvarlist", std::bind(&CvarSystem::CMD_CVARLIST_F, this, ph::_1, ph::_2), CMD_FLAG_NONE, "List cvars.", "cvarlist [filter]");

    // CVAR registration
    registerCvar(Cvar("name", std::string("Player"), 15, CVAR_FLAG_NONE, "Player name."));
}

void CvarSystem::initCvars()
{
    for (auto i : cvarList)
    {
        notifyChange(&i.second);
    }
}

void CvarSystem::registerCvar(Cvar cvar)
{
    cvar.cvarSystem = this;
    
    if (cvarList.count(cvar.getName()) > 0) {
    } else {
        cvarList.insert(std::make_pair(cvar.getName(), cvar));
    }
}

Cvar* CvarSystem::getCvar(const std::string& name)
{
    auto iter = cvarList.find(name);
    if (iter != cvarList.end()) {
        return &iter->second;
    } else {
        LOG(warn, "Tried to get cvar \"%s\" when it doesn't exist.", name.c_str());
        return &nullCvar;
    }
}

void CvarSystem::notifyChange(Cvar* cvar)
{   
    Event e{ EVENT_CVAR_CHANGED };
    e.data.push_back(cvar);
    sendEvent(e);
}

// CMD_CVARLIST_F
// List all cvars in the console.
void CvarSystem::CMD_CVARLIST_F(const Command*, const ArgList& args)
{
    if (args.count() == 0)
    {
        LOG(grey, "List of cvars (no filter):");

        for (auto i : cvarList)
        {
            LOG(white, "%-20s", i.first.c_str());
            LOGC(purple, "* ");
            LOGC(purple, "%s", i.second.getDescription().c_str());
        }
    }
    else {
        LOG(grey, "List of cvars (filter ");
        LOGC(green, "%s", args.at(0).c_str());
        LOGC(grey, "):");

        for (auto i : cvarList)
        {
            const size_t firstFound = i.first.find(args.at(0));
            if (firstFound != std::string::npos)
            {
                LOG(white, "%s", i.first.substr(0, firstFound).c_str());
                LOGC(green, "%s", args.at(0).c_str());
                LOGC(white, "%-*s", 20 - firstFound - args.at(0).length(), i.first.substr(firstFound + args.at(0).length(), std::string::npos).c_str());
                LOGC(purple, "* ");
                LOGC(purple, "%s", i.second.getDescription().c_str());
            }
        }
    }
}
