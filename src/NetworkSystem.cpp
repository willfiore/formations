#include "NetworkSystem.hpp"

#include "Console.hpp"
#include "CommandSystem.hpp"
#include "CvarSystem.hpp"
#include "GameState.hpp"
#include "Random.hpp"
#include "World.hpp"

#include <algorithm>

NetworkSystem::NetworkSystem() :
    m_isServer(false),
    m_isConnected(false),
    m_lastResendTime(sf::Time::Zero),
    m_lastPingTime(sf::Time::Zero),
    m_fakeLatency(sf::Time::Zero)
{
    broadcastClient = new NetClient();
    nullClient = new NetClient();
}

NetworkSystem::~NetworkSystem()
{
    delete broadcastClient;
    delete nullClient;
}

void NetworkSystem::EV_CVAR_CHANGED_F(const Event& e)
{
    Cvar* cvar = boost::any_cast<Cvar*>(e.data[0]);

    if (cvar->getName() == "name") {
        sf::Packet& msg = newMessage(NET_CHANGE_NICKNAME);
        msg << cvar->getValue_String();
    }
    else if (cvar->getName() == "net_port") {
        setPort(cvar->getValue_Int());
    }
    else if (cvar->getName() == "net_fakelatency") {
        m_fakeLatency = sf::milliseconds(cvar->getValue_Int());
    }
}

void NetworkSystem::EV_CMD_LAUNCH_F(const Event&)
{
    if(gameState->state != GAMESTATE_LOBBY) {
        LOG(red, "You must be in a lobby to launch the game.");
        return;
    }

    if(!m_isServer) {
        LOG(red, "You must be the host to launch the game.");
        return;
    }

    sf::Packet& msg = newMessage(NET_LAUNCH_GAME);
    // Will act as seed for sim number generator
    RandomGenerator::result_type simSeed = random->client.generate();
    msg << simSeed;
}

void NetworkSystem::sendTickDone(uint32_t tickNumber)
{
    sf::Packet& tickDoneMsg = newMessage(NET_TICK_DONE);
    tickDoneMsg << tickNumber;
}

void NetworkSystem::CMD_HOST_F(const Command*, const ArgList&)
{
    if (m_isConnected) {
        LOG(red, "You are already connected to a server.");
        return;
    }
    
    m_isServer = true;
    m_isConnected = true;
    m_clientList.clear();

    // Add self as client
    selfClient = addClient();
    selfClient->ip = sf::IpAddress::LocalHost;
    selfClient->port = socket.getLocalPort();
    selfClient->nickname = cvarSystem->getCvar("name")->getValue_String();
    hostClient = selfClient;

    sendEvent(EVENT_HOSTED_GAME);
}

void NetworkSystem::CMD_CONNECT_F(const Command* cmd, const ArgList& args)
{
    // Check for correct number of arguments
    if(args.count() != 2) {
        commandSystem->logHelpInfo(cmd);
        return;
    }

    sf::IpAddress address = sf::IpAddress(args.at(0));
    unsigned short port;

    // Sanity check port
    try { port = std::stoi(args.at(1)); }
    catch(const std::exception&) {
        LOG(red, "Invalid port number.");
        return;
    }

    connect(address, port);
}

void NetworkSystem::CMD_DISCONNECT_F(const Command*, const ArgList&)
{
    disconnect();
}

void NetworkSystem::CMD_CLIENTLIST_F(const Command*, const ArgList&)
{
    for(auto& client : m_clientList) {
        LOG(white, "[%u] %s (%ums)", client->clid, client->nickname.c_str(), client->latency.asMilliseconds());
    }
}

void NetworkSystem::CMD_SAY_F (const Command* cmd, const ArgList& args)
{
    if (args.count() < 1) {
        commandSystem->logHelpInfo(cmd);
        return;
    }

    // Concatenate args into single string
    std::string message;
    for (size_t i = 0; i < args.count(); ++i) {
        message.append(args.at(i) + " ");
    }

    // Send off packet
    sf::Packet& sayMessage = newMessage(NET_SAY);
    sayMessage << message;
}

void NetworkSystem::RECV_CL_CONNECT_F(sf::Packet& packet, const sf::IpAddress& ip, unsigned short port)
{
    // Extract packet data
    std::string nickname; if(!(packet >> nickname)) return;
    
    // Ignore if not a host
    if(!m_isServer) return;

    LOG(debug, "%u", gameState->state);

    // Ignore if not in lobby state
    if(gameState->state != GAMESTATE_LOBBY) {
        return;
    }

    // Ignore if already connected
    if (std::find_if(
            m_clientList.begin(),
            m_clientList.end(),
            [&](std::unique_ptr<NetClient> const& c) {
                return c->ip == ip && c->port == port;
            }) != m_clientList.end())
    {
        return;
    }

    // Add them as a client
    NetClient* newClient = addClient();
    newClient->ip = ip;
    newClient->port = port;
    newClient->nickname = formatNickname(newClient, nickname);

    // Tell everyone except them they have connected:
    for (auto& c : m_clientList) {
        if (c.get() == newClient) {
            continue;
        }
        sf::Packet& msg = newMessage(c.get(), NET_SV_CLIENT_CONNECTED);
        msg << newClient->clid << newClient->nickname;
    }

    // Tell the client they have connected:
    sf::Packet& acceptedMessage = newMessage(newClient, NET_SV_CONNECTION_ACCEPTED);
    // Append clientlist
    acceptedMessage << uint8_t(m_clientList.size());
    for (auto& client : m_clientList) {
        acceptedMessage << client->clid << client->nickname;
    }
}

void NetworkSystem::RECV_SV_CONNECTION_ACCEPTED_F(sf::Packet& packet, const sf::IpAddress& ip, unsigned short port)
{
    uint8_t numClients; if(!(packet >> numClients)) return;
    
    if(m_clientList.size() != 1) return;
    if(m_clientList[0]->ip != ip || m_clientList[0]->port != port) return;

    // Clear temp client
    m_clientList.clear();

    // Populate client list
    for(size_t i = 0; i < numClients; ++i) {
        NetClient* client = addClient();
        if(!(packet >> client->clid)) return;
        if(!(packet >> client->nickname)) return;

        // Set server to WAN ip
        if (i == 0) {
            client->ip = ip;
            client->port = port;
            hostClient = client;
        }

        if (i == numClients - 1) {
            selfClient = client;
        }
    }
    
    LOG(info, "Connected!");
    m_isConnected = true;
    sendEvent(EVENT_CONNECTED_TO_GAME);
}

void NetworkSystem::RECV_SV_CLIENT_CONNECTED_F(sf::Packet& packet)
{
    uint8_t clid; if (!(packet >> clid)) return;
    std::string nickname; if(!(packet >> nickname)) return;
    nickname = nickname.substr(0, (unsigned int)cvarSystem->getCvar("name")->getMaxValue());

    if(!m_isServer) {
        NetClient* newClient = addClient();
        newClient->clid = clid;
        newClient->nickname = nickname;
    }
    LOG(white, "%s connected.", nickname.c_str());
}

void NetworkSystem::RECV_SAY_F (const NetClient* client, sf::Packet& packet)
{
    // Print message!
    std::string message; if(!(packet >> message)) return;
    LOG(white, "[%s] ", client->nickname.c_str());
    LOGC(green, "%s", message.c_str());
}

void NetworkSystem::RECV_CHANGE_NICKNAME_F(NetClient* client, sf::Packet& packet)
{
    std::string nickname; if(!(packet >> nickname)) return;
    nickname = formatNickname(client, nickname);

    LOG(white, "%s changed name to %s.", client->nickname.c_str(), nickname.c_str());
    client->nickname = nickname;
}

void NetworkSystem::RECV_DISCONNECT_F(const NetClient* client, sf::Packet&)
{
    // Server disconnected:
    if(client == m_clientList[0].get()) {
        LOG(warn, "Server shut down.");
        sendEvent(EVENT_SERVER_DISCONNECTED);
        
        m_isConnected = false;
        m_clientList.clear();
        return;
    }

    // Client disconnected
    LOG(white, "%s disconnected.", client->nickname.c_str());
    removeClient(client);
}

void NetworkSystem::RECV_LAUNCH_GAME_F(const NetClient* client, sf::Packet& packet)
{
    // Sanity check
    if (client != hostClient) return;
    
    uint32_t simSeed; if(!(packet >> simSeed)) return;
    random->sim.seed(simSeed);

    sendEvent(EVENT_LAUNCH);
}

void NetworkSystem::RECV_TICK_DONE_F(NetClient* client, sf::Packet& packet)
{
    uint32_t tickNumber; if(!(packet >> tickNumber)) return;
    client->lastTick = tickNumber;

    // TODO: Push player inputs to WORLD SYSTEM
}

void NetworkSystem::RECV_SV_PING_F(sf::Packet& packet)
{
    // Send pong
    sf::Packet& pongMsg = newMessage(NET_CL_PONG);
    uint32_t timeSent; packet >> timeSent;
    pongMsg << timeSent;

    // Calculate ticks ahead required for smooth gameplay
    sf::Time highestPing {sf::Time::Zero};
    while(!packet.endOfPacket()) {
        uint8_t clid; packet >> clid;
        uint32_t latencyInt; packet >> latencyInt;
        
        sf::Time latency = sf::milliseconds(latencyInt);
        getClient(clid)->latency = latency;
        if (latency > highestPing) highestPing = latency;
    }
    gameState->ticksAhead = highestPing.asMilliseconds() / gameState->tickDelta.asMilliseconds();
    Event e(EVENT_GOT_HIGHEST_PING);
    e.data.push_back(highestPing);
    sendEvent(e);
}

void NetworkSystem::RECV_CL_PONG_F(NetClient* client, sf::Packet& packet)
{
    uint32_t timeSent; packet >> timeSent;
    client->latency = m_lastUpdateTime - sf::milliseconds(timeSent);
}

void NetworkSystem::init()
{
    socket.setBlocking(false);

    // CVARs
    cvarSystem->registerCvar(Cvar("net_port", 34111, 1024, 65535, CVAR_FLAG_NO_WRITE, "UDP port used for network communication."));
    cvarSystem->registerCvar(Cvar("net_fakelatency", 0, 0, 500, CVAR_FLAG_NONE, "Fake delay before sending / receiving packets (host only)"));

    // CMDs
    commandSystem->registerCmd("host",       std::bind(&NetworkSystem::CMD_HOST_F,       this, ph::_1, ph::_2), CMD_FLAG_NONE, "Host a networked multiplayer game.", "host");
    commandSystem->registerCmd("connect",    std::bind(&NetworkSystem::CMD_CONNECT_F,    this, ph::_1, ph::_2), CMD_FLAG_NONE, "Connect to a remote server.", "connect <ip> <port>");
    commandSystem->registerCmd("disconnect", std::bind(&NetworkSystem::CMD_DISCONNECT_F, this, ph::_1, ph::_2), CMD_FLAG_CONNECTED, "Disconnect from current game session.", "disconnect");
    commandSystem->registerCmd("clientlist", std::bind(&NetworkSystem::CMD_CLIENTLIST_F, this, ph::_1, ph::_2), CMD_FLAG_CONNECTED, "List connected clients.", "clientlist");
    commandSystem->registerCmd("say",        std::bind(&NetworkSystem::CMD_SAY_F,        this, ph::_1, ph::_2), CMD_FLAG_CONNECTED, "Chat.", "say <message>");

    // Event subscription
    std::vector<EventType> es{
        EVENT_CVAR_CHANGED,
            EVENT_CMD_LAUNCH
    };
    subscribe(es);
}

void NetworkSystem::setPort(unsigned short _port)
{
    socket.unbind();

    unsigned short port = _port;
    while (socket.bind(port) != sf::Socket::Done) {
        if (port == _port) {
            LOG(warn, "Failed to bind UDP socket to port %u. Trying another...", port);
        }
        port++;
    }
    cvarSystem->getCvar("net_port")->setValue_Int(port, true);
    LOG(info, "Bound UDP socket to port %u.", port);
}

sf::Packet& NetworkSystem::newMessage(MessageType type)
{
    if (m_clientList.size() > 0) {
        return newMessage(hostClient, type);
    }
    else {
        return newMessage(nullClient, type);
    }
}

sf::Packet& NetworkSystem::newMessage(NetClient* client, MessageType type)
{
    static uint16_t id = 0;

    NetMessage msg;
    msg.client = client;
    msg.id = id;
    msg.data = std::make_shared<sf::Packet>();
    msg.sentTime = sf::Time::Zero;

    *msg.data << uint8_t(type) << id;

    if (client == broadcastClient) {
        for (auto& c : m_clientList) {
            msg.client = c.get();
            m_messagesToSend.push_back(msg);
        }
    } else {
        m_messagesToSend.push_back(msg);
    }
    
    id++;
    return *msg.data;
}

NetClient* NetworkSystem::addClient()
{
    static uint8_t id = 0;
    if (m_clientList.empty()) id = 0;
    
    auto newClient = std::make_unique<NetClient>();
    NetClient* ptr = newClient.get();
    m_clientList.push_back(std::move(newClient));

    // Default setup
    ptr->clid = id;
    ptr->player = world->addPlayer();

    id++;
    return ptr;
}

void NetworkSystem::removeClient(const NetClient* client)
{
    auto iter = std::find_if(m_clientList.begin(),
                             m_clientList.end(),
                             [&](std::unique_ptr<NetClient> const& c) {
                                 return c->clid == client->clid;
                             });

    // Remove any messages pending to be sent:
    for (auto msg = m_messagesToSend.begin(); msg != m_messagesToSend.end();) {
        if (msg->client == (*iter).get()) {
            m_messagesToSend.erase(msg);
            continue;
        }
        msg++;
    }

    // Remove corresponding player
    world->removePlayer((*iter)->player);
    
    m_clientList.erase(iter);
}

NetClient* NetworkSystem::getClient(short clid)
{
    for (auto& c : m_clientList) {
        if (clid == c->clid) return c.get();
    }
    
    return broadcastClient;
}

std::string NetworkSystem::formatNickname(NetClient* client, std::string input)
{
    for (auto& c : m_clientList) {
        if (c.get() == client) continue; //ignore self
        if (input == c->nickname) {
            input = input + "*";
            input = formatNickname(client, input);
        }
    }
    return input;
}

void NetworkSystem::connect(const sf::IpAddress& ip, unsigned short port)
{
    // Disconnect from current server
    disconnect();
    
    // Create temporary client
    NetClient* tempClient = addClient();
    tempClient->ip   = ip;
    tempClient->port = port;

    // New message
    sf::Packet& msg = newMessage(tempClient, NET_CL_CONNECT);
    msg << cvarSystem->getCvar("name")->getValue_String();

    LOG(info, "Requesting connection to %s:%u...", ip.toString().c_str(), port);
}

void NetworkSystem::disconnect()
{
    if(m_isConnected) {
        newMessage(NET_DISCONNECT);
    }
    // Try and send disconnect packet - but really we want to disconnect
    // as fast as possible
    update(sf::Time::Zero, sf::Time::Zero);
    update(sf::Time::Zero, sf::Time::Zero);

    m_isServer = false;
    m_isConnected = false;
    m_clientList.clear();
    m_messagesToSend.clear();
    m_bufferedReceivedMessages.clear();
    selfClient = nullClient;
    hostClient = nullClient;

    sendEvent(EVENT_DISCONNECTED_FROM_GAME);
}

void NetworkSystem::update(sf::Time currentTime, sf::Time)
{
    m_lastUpdateTime = currentTime;
    // Check if all clients are ready to sim the next tick
    if(gameState->state == GAMESTATE_INGAME) {
        uint32_t currentTick = gameState->currentTick;
        bool shouldAdvance = true;
        for (auto& client : m_clientList) {
            if (client->lastTick < currentTick) {
                shouldAdvance = false;
            }
        }
        if (m_lastUpdateTime - gameState->currentTickBeginTime < gameState->tickDelta) {
            shouldAdvance = false;
        }
        if (shouldAdvance) {
            // Simulate next tick
            Event e(EVENT_READY_TO_SIM_NEXT_TICK);
            e.data.push_back(currentTime);
            sendEvent(e);
            // Send data for a later tick
            sendTickDone(currentTick + gameState->ticksAhead + 1);
        }
    }

    // Ping clients for latency
    if (m_isServer) {
        if (currentTime - m_lastPingTime > sf::milliseconds(500)) {
            m_lastPingTime = currentTime;
            sf::Packet& pingMsg = newMessage(broadcastClient, NET_SV_PING);
            pingMsg << currentTime.asMilliseconds();
            // Give previous ping values to all clients
            for (auto& c : m_clientList) {
                pingMsg << c->clid << c->latency.asMilliseconds();
            }
        }
    }
        
    // Sending packets
    for (auto msg = m_messagesToSend.begin(); msg != m_messagesToSend.end();) {
        // Remove packets to null clients
        if (msg->client == nullClient) {
            m_messagesToSend.erase(msg);
            continue;
        }
        
        // Send message, subject to fake latency conditions
        bool fakeLatencyHasElapsed = msg->fakeLatency.getElapsedTime().asMilliseconds() > m_fakeLatency.asMilliseconds()/2;
        if (!m_isServer || (m_isServer && msg->client == selfClient) || (m_isServer && fakeLatencyHasElapsed)) {
            // If message hasn't been sent yet, send it straight away
            if (msg->sentTime == sf::Time::Zero) {
                msg->sentTime = currentTime;
                socket.send(*msg->data, msg->client->ip, msg->client->port);
            }
            // Otherwise, resend it periodically
            else if (currentTime - m_lastResendTime > sf::milliseconds(200)) {
                m_lastResendTime = currentTime;
                socket.send(*msg->data, msg->client->ip, msg->client->port);
            }

            // If a message hasn't received a response in a while,
            // stop resending it.
            if (currentTime - msg->sentTime > sf::milliseconds(5000)) {
                m_messagesToSend.erase(msg);
                continue;
            }
        }
        msg++;
    }

    // Check if fake latency packets need reading
    for (auto msg = m_bufferedReceivedMessages.begin(); msg != m_bufferedReceivedMessages.end();) {
        if (msg->fakeLatency.getElapsedTime().asMilliseconds() > m_fakeLatency.asMilliseconds()/2) {
            onReceive(msg->packet, msg->address, msg->port);
            m_bufferedReceivedMessages.erase(msg);
            continue;
        }
        msg++;
    }

    // Receiving packets
    sf::Packet      rec_packet;
    sf::IpAddress   rec_address;
    unsigned short  rec_port;
    
    while (socket.receive(rec_packet, rec_address, rec_port) == sf::Socket::Done) {
        if (m_fakeLatency != sf::Time::Zero && m_isServer && (rec_address != selfClient->ip || rec_port != selfClient->port)) {
            NetReceivedMessage msg;
            msg.packet  = rec_packet;
            msg.address = rec_address;
            msg.port    = rec_port;
            m_bufferedReceivedMessages.push_back(msg);
        }
        else {
            onReceive(rec_packet, rec_address, rec_port);
        }
    }
}

void NetworkSystem::onReceive(sf::Packet& rec_packet, sf::IpAddress& rec_address, unsigned short& rec_port)
{
    uint8_t  type;
    uint16_t id;
    
    rec_packet >> type >> id;
        
    // Sanity check (if packet arrives corrupted, eg.)
    if (!rec_packet) return;

    // If it's not an ACK packet, send one.
    if (type != NET_ACK) {
        sf::Packet ack_packet;
        ack_packet << uint8_t(NET_ACK) << id;
        socket.send(ack_packet, rec_address, rec_port);
    }
    // It is an ACK, remove that packet from the send list.
    else {
        m_messagesToSend.erase(
            std::remove_if(
                m_messagesToSend.begin(),
                m_messagesToSend.end(),
                [&](NetMessage const& m) {
                    return m.id == id && m.client->ip == rec_address && m.client->port == rec_port;
                }),
            m_messagesToSend.end());
        return;
    }

    // Connection packets are the only clientless packets
    if(type == NET_CL_CONNECT) {
        RECV_CL_CONNECT_F(rec_packet, rec_address, rec_port);
        return;
    }
    else if(type == NET_SV_CONNECTION_ACCEPTED) {
        RECV_SV_CONNECTION_ACCEPTED_F(rec_packet, rec_address, rec_port);
        return;
    }
        
    // Find which client the packet corresponds to:
    auto clientIterator = std::find_if(
        m_clientList.begin(),
        m_clientList.end(),
        [&](std::unique_ptr<NetClient> const& c) {
            return c->ip == rec_address && c->port == rec_port;
        });

    // Ignore packets from unknown clients
    // Since non-server clients only have the IP of the server, this has the
    // added effect of ignoring any packet not from the server.
    if (clientIterator == m_clientList.end()) {
        LOG(warn, "Ignored packet from unknown IP.");
        return;
    }

    NetClient* client = clientIterator->get();

    // Ignore last 100 ACKed packets if they are sent again
    if ((id <= client->lastACK && client->lastACK - id < 100)         ||
        (id > client->lastACK && 65535 - id + client->lastACK < 100)) {
        return;
    }
    
    client->lastACK = id;

    // Check for server packets
    bool isServerPacket = true;
    switch(type) {
    case NET_SV_CLIENT_CONNECTED: RECV_SV_CLIENT_CONNECTED_F(rec_packet); break;
    case NET_SV_PING: RECV_SV_PING_F(rec_packet); break;
    default: isServerPacket = false; break;
    }
    if(isServerPacket) return;

    bool isToServerPacket = true;
    switch(type) {
    case NET_CL_PONG: RECV_CL_PONG_F(client, rec_packet); break;
    default: isToServerPacket = false; break;
    }
    if(isToServerPacket) return;

    // If I am the server, relay any messages from clients to everyone else
    // (if not already relayed)
    if (m_isServer && type != NET_SV_RELAY) {
        sf::Packet& relayMessage = newMessage(broadcastClient, NET_SV_RELAY);
        relayMessage << client->clid;
        relayMessage.append(rec_packet.getData(), rec_packet.getDataSize());
        return;
    }

    // All packets from here are NET_SV_RELAY prefixed: (emulating peer-to-peer)

    // Get the original packet header data
    uint8_t  clid;     rec_packet >> clid;
    uint8_t  trueType; rec_packet >> trueType;
    uint16_t trueID;   rec_packet >> trueID;
    
    NetClient* trueClient = getClient(clid);
        
    switch(trueType) {
    case NET_SAY:             RECV_SAY_F(trueClient, rec_packet);             break;
    case NET_CHANGE_NICKNAME: RECV_CHANGE_NICKNAME_F(trueClient, rec_packet); break;
    case NET_DISCONNECT:      RECV_DISCONNECT_F(trueClient, rec_packet);      break;
    case NET_LAUNCH_GAME:     RECV_LAUNCH_GAME_F(trueClient, rec_packet);     break;
    case NET_TICK_DONE:       RECV_TICK_DONE_F(trueClient, rec_packet);       break;
    default: break;
    }
}

void NetworkSystem::onEvent(const Event& e)
{
    switch (e.type)
    {
    case EVENT_INIT:
        init();
        break;
    case EVENT_EXIT:
        socket.unbind();
        break;
    case EVENT_CVAR_CHANGED: EV_CVAR_CHANGED_F(e); break;
    case EVENT_CMD_LAUNCH: EV_CMD_LAUNCH_F(e); break;
    default:
        break;          
    }
}
