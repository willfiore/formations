#include "World.hpp"

#include <SFML/Graphics/Rect.hpp>
#include <algorithm>

World::World()
{
}

Player* World::addPlayer()
{
    static uint8_t currentID = 0;
    
    Player player;
    player.id = currentID++;
    m_playerList.push_back(player);
    return &m_playerList.back();
}

void World::removePlayer(Player* player)
{
    m_playerList.erase(std::remove(m_playerList.begin(), m_playerList.end(), *player), m_playerList.end());
}

void World::createEmptyMap(int width, int height)
{
    m_tileMap.clear();
    m_tileMap.setDimensions(width, height);

    for (int j = 0; j < height; ++j) {
        for (int i = 0; i < width; ++i) {
            Tile tile;
            tile.id = i + (j * width);
            m_tileMap.getItems().push_back(tile);
        }
    }
}

void World::createTestMap(int width, int height)
{
    createEmptyMap(width, height);
    
    for (int i = 0; i < height; ++i) {
        for (int j = 0; j < width; ++j) {
            if ((!(j%14) && i%8) || (!(j%17) && i%7)) {
                m_tileMap.at(j, i).isWalkable = false;
            }
            if (!(i%13) && j%8) {
                m_tileMap.at(j, i).isWalkable = false;
            }
        }
    }
}

Unit* World::createUnit(sf::Vector2i position)
{
    static uint16_t currentID = 0;
    
    Unit unit;
    unit.id = currentID++;
    unit.setOldPosition(position);
    unit.setPosition(position);
    
    m_unitList.push_back(unit);
    return &m_unitList.back();
}

void World::removeUnit(Unit* unit)
{
    m_unitList.erase(std::remove(m_unitList.begin(), m_unitList.end(), *unit), m_unitList.end());
}
