#include "Pathfinder.hpp"
#include "Console.hpp"

#include <set>
#include <algorithm>

Pathfinder::Pathfinder()
{
    // Populate global LOCAL_TILES tile grid
    LOCAL_TILES.setDimensions(SECTOR_SIZE);
    for (size_t i = 0; i < LOCAL_TILES.getNumElements(); ++i) {
        LOCAL_TILES.at(i) = LocalTileID(i);
    }
}

void Pathfinder::init()
{
    tileMap = &world->getTileMap();
}

void Pathfinder::onEvent(const Event&)
{    
}

void Pathfinder::update()
{
}

void Pathfinder::initNewTileMap()
{
    // Reset sectors
    m_sectors.clear();
    int numSectorsX = (tileMap->getWidth() + SECTOR_SIZE - 1) / SECTOR_SIZE;
    int numSectorsY = (tileMap->getHeight() + SECTOR_SIZE - 1) / SECTOR_SIZE;
    m_sectors.setDimensions(numSectorsX, numSectorsY);
    // Set IDs, tiles
    for (int j = 0; j < numSectorsY; ++j) {
        for (int i = 0; i < numSectorsX; ++i) {
            Sector& sector = m_sectors.at(i, j);
            sector.id = (j*numSectorsX) + i;
            sector.tiles.setDimensions(SECTOR_SIZE, SECTOR_SIZE);
            for (int l = 0; l < SECTOR_SIZE; ++l) {
                for (int k = 0; k < SECTOR_SIZE; ++k) {
                    sector.tiles.at(k, l) = k + (l*tileMap->getWidth()) +
                        (i*SECTOR_SIZE) + (j*tileMap->getWidth()*SECTOR_SIZE);
                }
            }
        }
    }
    
    // Reset cost field
    generateCostField();
    
    m_integrationFields.clear();
    
    createPortals();
}

void Pathfinder::createPortals()
{
    nextFreePortalID = 0;
    
    for (size_t i = 0; i < m_sectors.getNumElements(); ++i) {
        createPortals(i);
    }
}

void Pathfinder::createPortals(SectorID sectorID)
{
    Sector* sector = &m_sectors.at(sectorID);
    auto neighbours = m_sectors.getNeighbours(sectorID);

    // Step 1: Remove all existing portals in this sector
    // WATCH OUT HERE: COULD SEG FAULT!!
    if (sector->portals.size() > 0) {
        sector->portals.clear();
        
        m_portals.erase(std::remove_if(m_portals.begin(),
                                       m_portals.end(),
                                       [sectorID](std::unique_ptr<Portal> const& p) {
                                           return sectorID == p->sector->id;
                                       }),
                        m_portals.end());
    }
    

    // Step 1: Locate portals
    bool lookingForStart = true;    
    std::vector<LocalTileID> tilesInCurrentPortal;
    
    for (int dir = Direction::UP; dir < 4; ++dir)
    {
        int xe;
        int ye;        
        int xm;
        int ym;
        int xm2;
        int ym2;

        if (neighbours.count(Direction(dir)) == 0) continue;
        SectorID adjacentSectorID = neighbours[Direction(dir)];
        
        lookingForStart = true;
        // dir changes which edge to iterate across
        switch(dir) {
        case Direction::UP: xe = 1; ye = 0; xm = 0; ym = 0; xm2 = 0; ym2 = SECTOR_SIZE - 1; break; // TOP
        case Direction::RIGHT: xe = 0; ye = 1; xm = SECTOR_SIZE - 1; ym = 0; xm2 = 0; ym2 = 0; break; // RIGHT
        case Direction::DOWN: xe = 1; ye = 0; xm = 0; ym = SECTOR_SIZE - 1; xm2 = 0; ym2 = 0; break; // BOTTOM
        case Direction::LEFT: xe = 0; ye = 1; xm = 0; ym = 0; xm2 = SECTOR_SIZE - 1; ym2 = 0; break; // LEFT
        }
        
        for(int i = 0; i < SECTOR_SIZE; ++i) {
            
            int x = (i*xe) + xm;
            int y = (i*ye) + ym;

            int x2 = (i*xe) + xm2;
            int y2 = (i*ye) + ym2;

            LocalTileID currentTile = LOCAL_TILES.at(x, y);
            LocalTileID adjacentTile = LOCAL_TILES.at(x2, y2);
            
            // If we find a wall:
            if (m_costField.at(localToGlobalTile(sectorID, currentTile)) == 255 ||
                m_costField.at(localToGlobalTile(adjacentSectorID, adjacentTile)) == 255) {
                if (lookingForStart) continue; // didn't find start - continue
                else {
                    // Create a new portal
                    m_portals.push_back(std::make_unique<Portal>());
                    sector->portals.push_back(m_portals.back().get());
                    Portal* portal = sector->portals.back();
                    portal->id = nextFreePortalID++;
                    portal->side = Direction(dir);
                    portal->tiles.swap(tilesInCurrentPortal);
                    tilesInCurrentPortal.clear();
                    lookingForStart = true;
                }
            } else { // Found passable tile
                tileMap->at(localToGlobalTile(sectorID, currentTile)).debug_Color = 1;
                tilesInCurrentPortal.push_back(currentTile);
                if (lookingForStart) { // found start!
                    lookingForStart = false;
                }
            }
        }

        // Reached end of edge. Finish creating a portal if already doing so:
        if (!lookingForStart) {
            m_portals.push_back(std::make_unique<Portal>());
            sector->portals.push_back(m_portals.back().get());
            Portal* portal = sector->portals.back();
            portal->id = nextFreePortalID++;
            portal->side = Direction(dir);
            portal->tiles.swap(tilesInCurrentPortal);
        }
    }
    
    formInnerPortalConnections(sectorID);
    formEdgePortalConnections(sectorID);
}

void Pathfinder::formInnerPortalConnections(SectorID sectorID)
{
    Sector* sector = &m_sectors.at(sectorID);
    for (auto& portal : sector->portals) {
        portal->connectedPortals.clear();
        tileMap->at(localToGlobalTile(sectorID, portal->getCenter())).debug_Color = 2;
        
        IntegrationField* integrationField = generateIntegrationField(sectorID, portal->getCenter());

        for (auto& portal2 : sector->portals) {
            // ignore self
            if(portal2->id == portal->id) continue;
            
            int distance = integrationField->field.at(portal2->getCenter()).cost;
            if(distance > 0) {
                // connected to this portal!
                portal->connectedPortals.push_back(std::make_pair(portal2, distance));
            }
        }
    }
}

void Pathfinder::formEdgePortalConnections(SectorID sectorID)
{
    Sector* sector = &m_sectors.at(sectorID);
    auto neighbours = m_sectors.getNeighbours(sectorID);
    int edgePortalDistance = 1; // KEEP AS 1. This is the defining feature of an edge portal.
    // No other portal can have a distance of 1 to another portal unless it is at an edge
    
    for (auto& portal : sector->portals) {

        // Remove all edge portals by checking against edgePortalDistance
        portal->connectedPortals.erase(std::remove_if(portal->connectedPortals.begin(),
                                                           portal->connectedPortals.end(),
                                                           [&](auto x) {
                                                               return x.second <= edgePortalDistance;
                                                           }),
                                            portal->connectedPortals.end());
        
        LocalTileID center = portal->getCenter();
        Sector* adjacentSector = &m_sectors.at(neighbours[portal->side]);

        LocalTileID adjacentCenter;
        
        switch(portal->side) {
        case Direction::UP:    adjacentCenter = LOCAL_TILES.at(LOCAL_TILES.getX(center), LOCAL_TILES.getHeight() - 1); break;
        case Direction::RIGHT: adjacentCenter = LOCAL_TILES.at(0, LOCAL_TILES.getY(center)); break;
        case Direction::DOWN:  adjacentCenter = LOCAL_TILES.at(LOCAL_TILES.getX(center), 0); break;
        case Direction::LEFT:  adjacentCenter = LOCAL_TILES.at(LOCAL_TILES.getWidth() - 1, LOCAL_TILES.getY(center)); break;
        default: adjacentCenter = -1; break;
        }

        for (auto& portal2 : adjacentSector->portals) {
            if (portal2->getCenter() != adjacentCenter) continue;
            // Found connected portal:

            // Add it
            portal->connectedPortals.push_back(std::make_pair(portal2, edgePortalDistance));
            
            portal2->connectedPortals.erase(std::remove_if(portal2->connectedPortals.begin(),
                                                           portal2->connectedPortals.end(),
                                                           [&](auto x) {
                                                               return x.second <= edgePortalDistance;
                                                           }),
                                            portal2->connectedPortals.end());
            
            portal2->connectedPortals.push_back(std::make_pair(portal, edgePortalDistance));

            //LOG(debug, "%u:(%u, %u) connected with %u:(%u, %u)", sectorID, LOCAL_TILES.getX(center), LOCAL_TILES.getY(center),
            //adjacentSector->id, LOCAL_TILES.getX(adjacentCenter), LOCAL_TILES.getY(adjacentCenter));
        }
    }
}

TileID Pathfinder::localToGlobalTile(SectorID sector, LocalTileID tile)
{
    return m_sectors.at(sector).tiles.at(tile);
}

void Pathfinder::generateCostField()
{
    m_costField.clear();
    m_costField.setDimensions(tileMap->getWidth(), tileMap->getHeight());
    
    for (size_t i = 0; i < m_sectors.getNumElements(); ++i) {
        generateCostField(i);
    }
}

void Pathfinder::generateCostField(SectorID id)
{
    Sector* sector = &m_sectors.at(id);
    for (size_t i = 0; i < sector->tiles.getNumElements(); ++i) {
        TileID tileID = sector->tiles.at(i);
        if (tileMap->at(tileID).isWalkable) {
            m_costField.at(tileID) = 1;
        } else {
            m_costField.at(tileID) = 255;
        }
    }
}

IntegrationField* Pathfinder::generateIntegrationField(SectorID sector, LocalTileID goal)
{
    m_integrationFields.emplace_back(std::make_unique<IntegrationField>());
    IntegrationField* integrationField = m_integrationFields.back().get();

    integrationField->field.setDimensions(SECTOR_SIZE, SECTOR_SIZE);
    integrationField->sector = sector;
    integrationField->goal = goal;

    std::set<TileID> checkedTiles;
    std::vector<TileID> currentTiles;
    std::vector<TileID> nextTiles;

    currentTiles.push_back(goal);
    checkedTiles.insert(goal);
    integrationField->field.at(goal).cost = 1;
    tileMap->at(localToGlobalTile(sector, goal)).debug_Color = 3;

    while (!currentTiles.empty()) {
        for (auto& tileID : currentTiles) {
            auto neighbourIDs = LOCAL_TILES.getNeighbours(tileID);

            for (auto& neighbourObject : neighbourIDs) {
                TileID neighbourID = neighbourObject.second;
                // Ignore already visited tiles unless they are significantly
                // more expensive
                if (checkedTiles.count(neighbourID) && (integrationField->field.at(neighbourID).cost - integrationField->field.at(tileID).cost) < 2) continue;
                // Ignore walls
                if (m_costField.at(localToGlobalTile(sector, neighbourID)) == 255) continue;

                integrationField->field.at(neighbourID).cost = m_costField.at(localToGlobalTile(sector, neighbourID)) + integrationField->field.at(tileID).cost;

                integrationField->field.at(neighbourID).parent = tileID;
                
                tileMap->at(localToGlobalTile(sector, neighbourID)).debug_integrationCost = integrationField->field.at(neighbourID).cost;

                checkedTiles.insert(neighbourID);
                nextTiles.push_back(neighbourID);
            }
        }
        currentTiles.swap(nextTiles);
        nextTiles.clear();
    }
    
    return integrationField;
}
