#include "FileSystem.hpp"

#include "Console.hpp"
#include "CvarSystem.hpp"

#include <fstream>

FileSystem::FileSystem() :
    cvarCfgName("user.cfg"),
    isReadingCvars(false)
{
}

void FileSystem::init()
{
    get_user_config_folder(configDir, sizeof(configDir), "Formations");
    if (configDir[0] == 0) {
        LOG(warn, "Unable to locate config folder. No settings will be saved.");
        return;
    }
    LOG(info, "Found config folder at %s.", configDir);

    subscribe(EVENT_CVAR_CHANGED);
}

void FileSystem::onEvent(const Event& e)
{
    switch(e.type) {
    case EVENT_CVAR_CHANGED: EV_CVAR_CHANGED_F(e); break;
    default: break;
    }
}

void FileSystem::EV_CVAR_CHANGED_F(const Event& e)
{
    Cvar* cvar = boost::any_cast<Cvar*>(e.data[0]);
    if (cvar->getFlags() & (CVAR_FLAG_READONLY | CVAR_FLAG_NO_WRITE)) return;
    
    writeCvarToFile(cvar);
}

void FileSystem::writeCvarToFile(Cvar* cvar)
{
    if (isReadingCvars) return;
    
    std::string f_in = configDir + cvarCfgName;
    std::string f_out = configDir + cvarCfgName + ".temp";

    std::ifstream configFile_in(f_in);
    std::ofstream configFile_temp(f_out);

    std::string line;
    bool wrote = false;
    while(std::getline(configFile_in, line)) {
        if(line.substr(0, line.find(" ")) != cvar->getName()) {
            configFile_temp << line << std::endl;
        }
        else if(cvar->getValue_String() != cvar->getValue_String_Default()) {
            configFile_temp << cvar->getName() << " " << cvar->getValue_String() << std::endl;
            wrote = true;
        }
        // Don't write default CVAR values
        else if(cvar->getValue_String() == cvar->getValue_String_Default()) {
            wrote = true;
        }
    }

    if (!wrote) {
        configFile_temp << cvar->getName() << " " << cvar->getValue_String() << std::endl;
    }

    configFile_in.close();
    configFile_temp.close();
    std::remove(f_in.c_str());
    std::rename(f_out.c_str(), f_in.c_str());
}

void FileSystem::readCvarsFromFile()
{
    isReadingCvars = true;
    
    std::string f = configDir + cvarCfgName;
    std::ifstream configFile(f);

    std::string line;
    while (std::getline(configFile, line))
    {
        std::string name = line.substr(0, line.find(" "));
        std::string value = line.substr(line.find(" ")+1);
        if (cvarSystem->exists(name)) {
            cvarSystem->getCvar(name)->setValue(value);
        }
    }

    isReadingCvars = false;
}
