#include "InputSystem.hpp"

#include "CvarSystem.hpp"
#include "CommandSystem.hpp"
#include "Console.hpp"

#include <imgui.h>
#include <imgui-SFML.h>

InputSystem::InputSystem()
{
}

void InputSystem::CMD_BIND_F(const Command* cmd, const ArgList& args)
{
    if (args.count() < 1) {
        commandSystem->logHelpInfo(cmd);
        return;
    }
    
    if (m_stringToKey.left.find(args.at(0)) != m_stringToKey.left.end()) {
        if (args.count() == 1) {
            LOG(white, args.at(0).c_str());
            if (m_keyBindings.count(m_stringToKey.left.at(args.at(0)))) {
                LOGC(grey, " is bound to ");
                LOGC(white, m_keyBindings[m_stringToKey.left.at(args.at(0))].c_str());
            } else {
                LOGC(grey, " is unbound.");
            }
        }
        else {
            m_keyBindings[m_stringToKey.left.at(args.at(0))] = args.at(1);
        }
        
    }
    else if (m_stringToMouseButton.left.find(args.at(0)) != m_stringToMouseButton.left.end()) {
        if (args.count() == 1) {
            LOG(white, args.at(0).c_str());
            if (m_mouseBindings.count(m_stringToMouseButton.left.at(args.at(0)))) {
                LOGC(grey, " is bound to ");
                LOGC(white, m_mouseBindings[m_stringToMouseButton.left.at(args.at(0))].c_str());
            } else {
                LOGC(grey, " is unbound.");
            }
        }
        else {
            m_mouseBindings[m_stringToMouseButton.left.at(args.at(0))] = args.at(1);
        }
    }
    else {
        LOG(red, "%s is not a key.", args.at(0).c_str());
        return;
    }
}

void InputSystem::CMD_UNBIND_F(const Command* cmd, const ArgList& args)
{
    if (args.count() < 1) {
        commandSystem->logHelpInfo(cmd);
        return;
    }

    if (m_stringToKey.left.find(args.at(0)) != m_stringToKey.left.end()) {
        m_keyBindings.erase(m_stringToKey.left.at(args.at(0)));
    }
    else if (m_stringToMouseButton.left.find(args.at(0)) != m_stringToMouseButton.left.end()) {
        m_mouseBindings.erase(m_stringToMouseButton.left.at(args.at(0)));
    }
    else {
        LOG(red, "%s is not a key.", args.at(0).c_str());
    }
}

void InputSystem::init()
{
    // Init keyboard keys
    m_stringToKey.insert(string_keymap::value_type("a",         sf::Keyboard::Key::A));
    m_stringToKey.insert(string_keymap::value_type("b",         sf::Keyboard::Key::B));
    m_stringToKey.insert(string_keymap::value_type("c",         sf::Keyboard::Key::C));
    m_stringToKey.insert(string_keymap::value_type("d",         sf::Keyboard::Key::D));
    m_stringToKey.insert(string_keymap::value_type("e",         sf::Keyboard::Key::E));
    m_stringToKey.insert(string_keymap::value_type("f",         sf::Keyboard::Key::F));
    m_stringToKey.insert(string_keymap::value_type("g",         sf::Keyboard::Key::G));
    m_stringToKey.insert(string_keymap::value_type("h",         sf::Keyboard::Key::H));
    m_stringToKey.insert(string_keymap::value_type("i",         sf::Keyboard::Key::I));
    m_stringToKey.insert(string_keymap::value_type("j",         sf::Keyboard::Key::J));
    m_stringToKey.insert(string_keymap::value_type("k",         sf::Keyboard::Key::K));
    m_stringToKey.insert(string_keymap::value_type("l",         sf::Keyboard::Key::L));
    m_stringToKey.insert(string_keymap::value_type("m",         sf::Keyboard::Key::M));
    m_stringToKey.insert(string_keymap::value_type("n",         sf::Keyboard::Key::N));
    m_stringToKey.insert(string_keymap::value_type("o",         sf::Keyboard::Key::O));
    m_stringToKey.insert(string_keymap::value_type("p",         sf::Keyboard::Key::P));
    m_stringToKey.insert(string_keymap::value_type("q",         sf::Keyboard::Key::Q));
    m_stringToKey.insert(string_keymap::value_type("r",         sf::Keyboard::Key::R));
    m_stringToKey.insert(string_keymap::value_type("s",         sf::Keyboard::Key::S));
    m_stringToKey.insert(string_keymap::value_type("t",         sf::Keyboard::Key::T));
    m_stringToKey.insert(string_keymap::value_type("u",         sf::Keyboard::Key::U));
    m_stringToKey.insert(string_keymap::value_type("v",         sf::Keyboard::Key::V));
    m_stringToKey.insert(string_keymap::value_type("w",         sf::Keyboard::Key::W));
    m_stringToKey.insert(string_keymap::value_type("x",         sf::Keyboard::Key::X));
    m_stringToKey.insert(string_keymap::value_type("y",         sf::Keyboard::Key::Y));
    m_stringToKey.insert(string_keymap::value_type("z",         sf::Keyboard::Key::Z));
    m_stringToKey.insert(string_keymap::value_type("0",         sf::Keyboard::Key::Num0));
    m_stringToKey.insert(string_keymap::value_type("1",         sf::Keyboard::Key::Num1));
    m_stringToKey.insert(string_keymap::value_type("2",         sf::Keyboard::Key::Num2));
    m_stringToKey.insert(string_keymap::value_type("3",         sf::Keyboard::Key::Num3));
    m_stringToKey.insert(string_keymap::value_type("4",         sf::Keyboard::Key::Num4));
    m_stringToKey.insert(string_keymap::value_type("5",         sf::Keyboard::Key::Num5));
    m_stringToKey.insert(string_keymap::value_type("6",         sf::Keyboard::Key::Num6));
    m_stringToKey.insert(string_keymap::value_type("7",         sf::Keyboard::Key::Num7));
    m_stringToKey.insert(string_keymap::value_type("8",         sf::Keyboard::Key::Num8));
    m_stringToKey.insert(string_keymap::value_type("9",         sf::Keyboard::Key::Num9));
    m_stringToKey.insert(string_keymap::value_type("f1",        sf::Keyboard::Key::F1));
    m_stringToKey.insert(string_keymap::value_type("f2",        sf::Keyboard::Key::F2));
    m_stringToKey.insert(string_keymap::value_type("f3",        sf::Keyboard::Key::F3));
    m_stringToKey.insert(string_keymap::value_type("f4",        sf::Keyboard::Key::F4));
    m_stringToKey.insert(string_keymap::value_type("f5",        sf::Keyboard::Key::F5));
    m_stringToKey.insert(string_keymap::value_type("f6",        sf::Keyboard::Key::F6));
    m_stringToKey.insert(string_keymap::value_type("f7",        sf::Keyboard::Key::F7));
    m_stringToKey.insert(string_keymap::value_type("f8",        sf::Keyboard::Key::F8));
    m_stringToKey.insert(string_keymap::value_type("f9",        sf::Keyboard::Key::F9));
    m_stringToKey.insert(string_keymap::value_type("f10",       sf::Keyboard::Key::F10));
    m_stringToKey.insert(string_keymap::value_type("f11",       sf::Keyboard::Key::F11));
    m_stringToKey.insert(string_keymap::value_type("f12",       sf::Keyboard::Key::F12));
    m_stringToKey.insert(string_keymap::value_type("f13",       sf::Keyboard::Key::F13));
    m_stringToKey.insert(string_keymap::value_type("f14",       sf::Keyboard::Key::F14));
    m_stringToKey.insert(string_keymap::value_type("f15",       sf::Keyboard::Key::F15));
    m_stringToKey.insert(string_keymap::value_type("\\",        sf::Keyboard::Key::BackSlash));
    m_stringToKey.insert(string_keymap::value_type("backspace", sf::Keyboard::Key::BackSpace));
    m_stringToKey.insert(string_keymap::value_type(",",         sf::Keyboard::Key::Comma));
    m_stringToKey.insert(string_keymap::value_type("-",         sf::Keyboard::Key::Dash));
    m_stringToKey.insert(string_keymap::value_type("del",       sf::Keyboard::Key::Delete));
    m_stringToKey.insert(string_keymap::value_type("num/",      sf::Keyboard::Key::Divide));
    m_stringToKey.insert(string_keymap::value_type("down",      sf::Keyboard::Key::Down));
    m_stringToKey.insert(string_keymap::value_type("end",       sf::Keyboard::Key::End));
    m_stringToKey.insert(string_keymap::value_type("esc",       sf::Keyboard::Key::Escape));
    m_stringToKey.insert(string_keymap::value_type("home",      sf::Keyboard::Key::Home));
    m_stringToKey.insert(string_keymap::value_type("ins",       sf::Keyboard::Key::Insert));
    m_stringToKey.insert(string_keymap::value_type("alt",       sf::Keyboard::Key::LAlt));
    m_stringToKey.insert(string_keymap::value_type("[",         sf::Keyboard::Key::LBracket));
    m_stringToKey.insert(string_keymap::value_type("ctrl",      sf::Keyboard::Key::LControl));
    m_stringToKey.insert(string_keymap::value_type("shift",     sf::Keyboard::Key::LShift));
    //m_stringToKey.insert(string_keymap::value_type("super",     sf::Keyboard::Key::LSystem));
    m_stringToKey.insert(string_keymap::value_type("left",      sf::Keyboard::Key::Left));
    m_stringToKey.insert(string_keymap::value_type("num*",      sf::Keyboard::Key::Multiply));
    m_stringToKey.insert(string_keymap::value_type("num0",      sf::Keyboard::Key::Numpad0));
    m_stringToKey.insert(string_keymap::value_type("num1",      sf::Keyboard::Key::Numpad1));
    m_stringToKey.insert(string_keymap::value_type("num2",      sf::Keyboard::Key::Numpad2));
    m_stringToKey.insert(string_keymap::value_type("num3",      sf::Keyboard::Key::Numpad3));
    m_stringToKey.insert(string_keymap::value_type("num4",      sf::Keyboard::Key::Numpad4));
    m_stringToKey.insert(string_keymap::value_type("num5",      sf::Keyboard::Key::Numpad5));
    m_stringToKey.insert(string_keymap::value_type("num6",      sf::Keyboard::Key::Numpad6));
    m_stringToKey.insert(string_keymap::value_type("num7",      sf::Keyboard::Key::Numpad7));
    m_stringToKey.insert(string_keymap::value_type("num8",      sf::Keyboard::Key::Numpad8));
    m_stringToKey.insert(string_keymap::value_type("num9",      sf::Keyboard::Key::Numpad9));
    m_stringToKey.insert(string_keymap::value_type("pgdn",      sf::Keyboard::Key::PageDown));
    m_stringToKey.insert(string_keymap::value_type("pgup",      sf::Keyboard::Key::PageUp));
    m_stringToKey.insert(string_keymap::value_type(".",         sf::Keyboard::Key::Period));
    m_stringToKey.insert(string_keymap::value_type("'",         sf::Keyboard::Key::Quote));
    m_stringToKey.insert(string_keymap::value_type("ralt",      sf::Keyboard::Key::RAlt));
    m_stringToKey.insert(string_keymap::value_type("]",         sf::Keyboard::Key::RBracket));
    m_stringToKey.insert(string_keymap::value_type("rctrl",     sf::Keyboard::Key::RControl));
    m_stringToKey.insert(string_keymap::value_type("rshift",    sf::Keyboard::Key::RShift));
    //m_stringToKey.insert(string_keymap::value_type("rsuper",    sf::Keyboard::Key::RSystem));
    m_stringToKey.insert(string_keymap::value_type("enter",     sf::Keyboard::Key::Return));
    m_stringToKey.insert(string_keymap::value_type("right",     sf::Keyboard::Key::Right));
    m_stringToKey.insert(string_keymap::value_type(";",         sf::Keyboard::Key::SemiColon));
    m_stringToKey.insert(string_keymap::value_type("/",         sf::Keyboard::Key::Slash));
    m_stringToKey.insert(string_keymap::value_type("space",     sf::Keyboard::Key::Space));
    m_stringToKey.insert(string_keymap::value_type("num-",      sf::Keyboard::Key::Subtract));
    m_stringToKey.insert(string_keymap::value_type("tab",       sf::Keyboard::Key::Tab));
    //m_stringToKey.insert(string_keymap::value_type("tilde",     sf::Keyboard::Key::Tilde));
    m_stringToKey.insert(string_keymap::value_type("up",        sf::Keyboard::Key::Up));

    m_stringToMouseButton.insert(string_mousemap::value_type("mouse1", sf::Mouse::Button::Left));
    m_stringToMouseButton.insert(string_mousemap::value_type("mouse3", sf::Mouse::Button::Middle));
    m_stringToMouseButton.insert(string_mousemap::value_type("mouse2", sf::Mouse::Button::Right));
    m_stringToMouseButton.insert(string_mousemap::value_type("mouse4", sf::Mouse::Button::XButton1));
    m_stringToMouseButton.insert(string_mousemap::value_type("mouse5", sf::Mouse::Button::XButton2));
    
    commandSystem->registerCmd("bind", std::bind(&InputSystem::CMD_BIND_F, this, ph::_1, ph::_2), CMD_FLAG_NONE, "Bind a key to a console command.", "bind <key> [command]");
    commandSystem->registerCmd("unbind", std::bind(&InputSystem::CMD_UNBIND_F, this, ph::_1, ph::_2), CMD_FLAG_NONE, "Unbind a key.", "unbind <key>");

    // Default keys
    commandSystem->executeCmdString("bind mouse1 +select");
    commandSystem->executeCmdString("bind 1 host");
    commandSystem->executeCmdString("bind 2 \"connect localhost 34111\"");
    commandSystem->executeCmdString("bind 3 launch");
}

void InputSystem::addKeyBinding(sf::Keyboard::Key key, const std::string& cmd)
{
    m_keyBindings[key] = cmd;
}
void InputSystem::addMouseBinding(sf::Mouse::Button button, const std::string& cmd)
{
    m_mouseBindings[button] = cmd;
}

void InputSystem::handleInput(sf::Event& e)
{
    ImGui::SFML::ProcessEvent(e);

    if (e.type == sf::Event::Closed) {
        commandSystem->executeCmdString("quit");
    }
    if (e.type == sf::Event::Resized) {
        Event ev(EVENT_WINDOW_RESIZED);
        ev.data.push_back(e.size.width);
        ev.data.push_back(e.size.height);
        sendEvent(ev);
    }
    
    if (e.type == sf::Event::KeyPressed && e.key.code == sf::Keyboard::Key::Tilde) {
        // Toggle console visibility
        CONSOLE.setVisibility(-1);
        return;
    }
    
    // If debug console is open, don't process other commands
    if (CONSOLE.isVisible()) {
        return;
    }

    // Key presses
    if (e.type == sf::Event::KeyPressed) {
        if (!m_keyBindings.count(e.key.code)) return;
        commandSystem->executeCmdString(m_keyBindings[e.key.code]);
        return;
    }
    // Mouse presses
    else if (e.type == sf::Event::MouseButtonPressed) {
        if (!m_mouseBindings.count(e.mouseButton.button)) return;
        commandSystem->executeCmdString(m_mouseBindings[e.mouseButton.button]);
        return;
    }
    // Key releases
    else if (e.type == sf::Event::KeyReleased) {
        if (!m_keyBindings.count(e.key.code)) return;
        if (m_keyBindings[e.key.code].front() != '+') return;
        std::string releaseCmd = m_keyBindings[e.key.code];
        releaseCmd.front() = '-';
        commandSystem->executeCmdString(releaseCmd);
        return;
    }
    // Mouse releases
    else if (e.type == sf::Event::MouseButtonReleased) {
        if (!m_mouseBindings.count(e.mouseButton.button)) return;
        if (m_mouseBindings[e.mouseButton.button].front() != '+') return;
        std::string releaseCmd = m_mouseBindings[e.mouseButton.button];
        releaseCmd.front() = '-';
        commandSystem->executeCmdString(releaseCmd);
    }
}

void InputSystem::onEvent(const Event& e)
{
    switch(e.type) {
    case EVENT_INIT: init(); break;
    default: break;
    }
}
