#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include <SFML/System/Vector2.hpp>

class GameObject
{
public:
    sf::Vector2i getPosition() const     { return position; }
    sf::Vector2i getOldPosition() const { return position_old; }
    sf::Vector2i getDeltaPosition() const { return position - position_old; }
    
    void setPosition(sf::Vector2i p)     { position = p; }
    void setOldPosition(sf::Vector2i p) { position_old = p; }

    void changePosition(int dx, int dy) { position.x += dx; position.y += dy; }
private:
    sf::Vector2i position_old;
    sf::Vector2i position;
};

#endif
