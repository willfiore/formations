#ifndef COMMANDSYSTEM_H
#define COMMANDSYSTEM_H

#include <string>
#include <functional>
#include <map>

#include "Placeholders.hpp"
#include "Console.hpp"
#include "Command.hpp"

class CvarSystem;
class GameState;

class CommandSystem
{
public:
    CommandSystem();
    void inject(GameState* _g, CvarSystem* _c) {
        gameState = _g;
        cvarSystem = _c;
    }
    void init();

    void registerCmd(const std::string& name, const std::function<void(const Command* cmd, const ArgList& args)>& func, int flags, const std::string& desc, const std::string& usage);
    void executeCmdString(std::string cmd);
    void logHelpInfo(const Command* cmd);
    bool exists(const std::string cmd) { return cmdList.count(cmd) > 0; }

private:
    void execute(const Command* cmd, const ArgList& args);

    std::map<std::string, Command> cmdList;
        
    void CMD_CMDLIST_F(const Command*, const ArgList&);

    GameState* gameState;
    CvarSystem* cvarSystem;
};

#endif
