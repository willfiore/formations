#ifndef NETMESSAGE_H
#define NETMESSAGE_H

#include <SFML/Network/IpAddress.hpp>
#include <SFML/Network/Packet.hpp>
#include <SFML/System/Clock.hpp>
#include "NetClient.hpp"
#include <memory>

struct NetMessage
{
    NetClient* client;
    uint16_t   id;
    std::shared_ptr<sf::Packet> data;
    sf::Clock  fakeLatency;
    sf::Time   sentTime;
};

struct NetReceivedMessage
{
    sf::Packet     packet;
    sf::IpAddress  address;
    unsigned short port;
    sf::Clock      fakeLatency;
};

#endif
