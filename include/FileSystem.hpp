#ifndef FILESYSTEM_H
#define FILESYSTEM_H

#include "EventListener.hpp"

#include "cfgpath.h"

#include <string>

class Cvar;
class CvarSystem;

class FileSystem : public EventListener
{
public:
    FileSystem();
    void inject(CvarSystem* _c) {
        cvarSystem = _c;
    }
    void init();
    virtual void onEvent(const Event& e) override;
    void readCvarsFromFile();
private:
    void writeCvarToFile(Cvar* cvar);

    bool isReadingCvars;

    char configDir[MAX_PATH];
    std::string cvarCfgName;

    void EV_CVAR_CHANGED_F(const Event&);
    
    CvarSystem* cvarSystem;
};

#endif
