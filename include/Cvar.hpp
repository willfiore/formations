#ifndef CVAR_H
#define CVAR_H

#include <string>

class CvarSystem;

enum class CvarType
{
    Int,
        Float,
        Bool,
        String
        };

enum CvarFlags
{
    CVAR_FLAG_NONE     = 0,
    CVAR_FLAG_CHEAT    = 1 << 0,
    CVAR_FLAG_READONLY = 1 << 1,
    CVAR_FLAG_NO_WRITE = 1 << 2
};

class Cvar
{
public:
    friend CvarSystem;
    
    Cvar(std::string _name, std::string _defaultValue, int _maxchars, int flags, std::string _description);
    Cvar(std::string _name, int _defaultValue, float _min, float _max, int flags, std::string _description);
    Cvar(std::string _name, float _defaultValue, float _min, float _max, int flags, std::string _description);
    Cvar(std::string _name, bool _defaultValue, int flags, std::string _description);



    std::string getName() const { return name; }
    std::string getDescription() const { return description; }
        
    CvarType getType() const { return type; };

    float getMinValue() const { return minValue; }
    float getMaxValue() const { return maxValue; }

    std::string getValue_String() const { return value; }
    std::string getValue_String_Default() const { return defaultValue; }
    int         getValue_Int() const { return std::stoi(value); }
    float       getValue_Float() const { return std::stof(value); }
    bool        getValue_Bool() const { return std::stoi(value) > 0; }

    void logInfoString() const;

    int getFlags() const { return flags; };

    bool isSettable() const;

    // Generic set value, used when type of value provided
    // is unknown (read: user provided)
    // (performs sanity checks)
    void setValue        (std::string v) { setValue        (v, false); }
    void setValue_String (std::string v) { setValue_String (v, false); }
    void setValue_Int    (const int v)   { setValue_Int    (v, false); }
    void setValue_Float  (const float v) { setValue_Float  (v, false); }
    void setValue_Bool   (const bool v)  { setValue_Bool   (v, false); }

    void setValue        (std::string, bool silent);
    void setValue_String (std::string, bool);
    void setValue_Int    (const int, bool);
    void setValue_Float  (const float, bool);
    void setValue_Bool   (const bool, bool);

private:
    std::string name;
    std::string description;
    CvarType    type;

    float       minValue = 0;
    float       maxValue = 0;
    std::string defaultValue;
    std::string value;
    int         flags;

    CvarSystem* cvarSystem;
};

#endif
