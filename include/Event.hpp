#ifndef EVENT_H
#define EVENT_H

#include <vector>
#include <boost/any.hpp>

enum EventType
{
    EVENT_INIT,
    EVENT_EXIT,
    EVENT_WINDOW_RESIZED,
    EVENT_CVARS_ALERT_INIT_VALUES,
    EVENT_CVAR_CHANGED,
    EVENT_HOSTED_GAME,
    EVENT_CONNECTED_TO_GAME,
    EVENT_DISCONNECTED_FROM_GAME,
    EVENT_SERVER_DISCONNECTED,
    EVENT_GOT_HIGHEST_PING,
    EVENT_CMD_LAUNCH,
    EVENT_LAUNCH,
    EVENT_READY_TO_SIM_NEXT_TICK,
    EVENT_CLIENT_CREATED,
    EVENT_CLIENT_DESTROYED,
    EVENT_SELECTION_BOX,

    EVENT_NULL_TYPE
};

class Event
{
public:
    Event() 
    {
        type = EVENT_NULL_TYPE;
    }
    Event(EventType _type)
    {
        type = _type;
    }

    EventType type;
    std::vector<boost::any> data;
};

#endif
