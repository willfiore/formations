#ifndef RANDOMGENERATOR_H
#define RANDOMGENERATOR_H

#include <cstdint>

class well512 {
public:
    typedef uint32_t result_type;
    static constexpr result_type min() { return 0; }
    static constexpr result_type max() { return UINT32_MAX; }
    result_type operator()() {
        result_type a, b, c, d;
        a = state[index];
        c = state[(index+13)&15];
        b = a^c^(a<<16)^(c<<15);
        c = state[(index+9)&15];
        c ^= (c>>11);
        a = state[index] = b^c;
        d = a^((a<<5)&0xDA442D24UL);
        index = (index + 15)&15;
        a = state[index];
        state[index] = a^b^d^(a<<2)^(b<<18)^(c<<28);
        return state[index];
    }

    result_type seed;
    result_type state[16];
    unsigned int index = 0;
};

class RandomGenerator {
public:
    RandomGenerator();
    typedef well512::result_type result_type;
    void seed();
    void seed(result_type);
    result_type getSeed();
    result_type generate();
    result_type generate(int min, int max);
private:
    well512 engine;
};

#endif
