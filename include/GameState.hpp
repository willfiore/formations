#ifndef GAMESTATE_H
#define GAMESTATE_H

#include <SFML/System/Time.hpp>
#include <stdint.h>

enum GameStateType {
    GAMESTATE_NONE   = 1 << 0,
    GAMESTATE_MENU   = 1 << 1,
    GAMESTATE_LOBBY  = 1 << 2,
    GAMESTATE_INGAME = 1 << 3,
    GAMESTATE_PAUSED = 1 << 4
};

class GameState
{
public:
    inline GameState() :
        state(GAMESTATE_NONE),
        currentTick(0),
        ticksAhead(0),
        currentTickBeginTime(sf::Time::Zero),
        tickDelta(sf::milliseconds(100))
    {}
    
    GameStateType state;
    uint32_t      currentTick;
    uint32_t      ticksAhead;
    sf::Time      currentTickBeginTime;
    sf::Time      tickDelta;
};

#endif
