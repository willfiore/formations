#ifndef EVENTMANAGER_H
#define EVENTMANAGER_H

#include "Event.hpp"
#include <map>
#include <vector>

#include "Console.hpp"

class EventListener;

class EventManager
{
public:
    EventManager();

    void addSubscriber(EventType, EventListener*);
    void removeSubscriber(EventType, EventListener*);

    void sendEvent(Event);
    void sendEvent(EventType);

private:
    std::map<EventType, std::vector<EventListener*>> subscriberList;
};

#endif
