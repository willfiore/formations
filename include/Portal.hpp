#ifndef PORTAL_H
#define PORTAL_H

#include "Tile.hpp"
#include "Sector.hpp"

#include <vector>

// Note: PortalIDs are global!
// (rather than local to a sector)
typedef int PortalID;

struct Portal
{
    Sector* sector;
    PortalID id;
    Direction side;
    std::vector<LocalTileID> tiles;
    
    std::vector<std::pair<Portal*, int> > connectedPortals;

    LocalTileID getCenter() {
        return tiles[tiles.size()/2];
    }
};

#endif
