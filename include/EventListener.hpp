#ifndef EVENTLISTENER_H
#define EVENTLISTENER_H

#include "Event.hpp"

class EventManager;

class EventListener
{
public:
    EventListener();
    void initEventManager(EventManager*);
    virtual void onEvent(const Event& e) = 0;
    
protected:
    // Event Manager helper methods
    void subscribe(EventType e);
    void subscribe(const std::vector<EventType>& v);
    void unsubscribe(EventType e);
    
    void sendEvent(const Event& e);
    void sendEvent(EventType t);
    
private:
    EventManager* el_eventManager;
};

#endif
