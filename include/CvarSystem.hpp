#ifndef CVARSYSTEM_H
#define CVARSYSTEM_H

#include "EventListener.hpp"
#include "Cvar.hpp"

#include <map>
#include <string>

class ArgList;
class Command;
class CommandSystem;

class CvarSystem : public EventListener
{
public:
    CvarSystem();
    void inject(CommandSystem* _c) {
        commandSystem = _c;
    }
    void init();
    void initCvars();

    void  registerCvar(Cvar);
    Cvar* getCvar(const std::string& name);
    bool  exists(const std::string& n) const { return cvarList.count(n) > 0; }
    
    void notifyChange(Cvar* cvar);

    void CMD_CVARLIST_F(const Command*, const ArgList&);
    void CMD_SET_F(const Command*, const ArgList&);
    void CMD_GET_F(const Command*, const ArgList&);

    virtual void onEvent(const Event&) override;

private:
    
    std::map<std::string, Cvar> cvarList;

    Cvar nullCvar;
    CommandSystem* commandSystem;
};

#endif
