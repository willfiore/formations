#ifndef SECTOR_H
#define SECTOR_H

#include "TwoVector.hpp"
#include "Tile.hpp"

#include <vector>
#include <memory>

struct Portal;

typedef int SectorID;

struct Sector
{
    SectorID id;
    TwoVector<TileID> tiles;
    std::vector<Portal*> portals;
};

#endif
