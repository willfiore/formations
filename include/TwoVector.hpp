#ifndef TWOVECTOR_H
#define TWOVECTOR_H

#include "Console.hpp"

#include <vector>
#include <map>

enum Direction {
    UP,
    RIGHT,
    DOWN,
    LEFT
};

template<typename T>
class TwoVector
{
public:
    TwoVector();
    
    int getWidth() const { return m_width; }
    int getHeight() const { return m_height; }
    typename std::vector<T>::size_type getNumElements() const { return m_width * m_height; }

    std::vector<T> const& getItems() const { return m_items; }
    std::vector<T>& getItems() { return m_items; }

    int coordToId(int x, int y) const { return x + (y * m_width); }
    int getX(int id) const { return id % m_width; }
    int getY(int id) const { return id / m_width; }

    T const& at(int id) const;
    T const& at(int x, int y) const;

    T& at(int id);
    T& at(int x, int y);

    int getNeighbour(int id, Direction) const;
    std::map<Direction, int> getNeighbours(int id) const;

    void clear();
    void setDimensions(int x, int y);
    void setDimensions(int xy);
    
private:
    std::vector<T> m_items;
    int m_width;
    int m_height;
};

template<typename T>
TwoVector<T>::TwoVector() :
    m_width(0),
    m_height(0)
{
}

template<typename T>
T const& TwoVector<T>::at(int id) const
{
    return m_items[id];
}

template<typename T>
T const& TwoVector<T>::at(int x, int y) const
{
    return m_items[coordToId(x, y)];
}

template <typename T>
T& TwoVector<T>::at(int id)
{
    return m_items[id];
}

template<typename T>
T& TwoVector<T>::at(int x, int y)
{
    return m_items[coordToId(x, y)];
}

template<typename T>
int TwoVector<T>::getNeighbour(int id, Direction dir) const
{
    int x = id % m_width;
    int y = id / m_width;
    
    switch (dir) {
    case Direction::UP:
        if (y > 0) {
            return coordToId(x, y-1);
        }
        break;
    case Direction::RIGHT:
        if (x < m_width - 1) {
            return coordToId(x+1, y);
        }
        break;
    case Direction::DOWN:
        if (y < m_height - 1) {
            return coordToId(x, y+1);
        }
        break;
    case Direction::LEFT:
        if (x > 0) {
            return coordToId(x-1, y);
        }
        break;
    }

    return -1;
}

template<typename T>
std::map<Direction, int> TwoVector<T>::getNeighbours(int id) const
{   
    std::map<Direction, int> neighbours;
    int x = id % m_width;
    int y = id / m_width;
    
    if (x >= getWidth() && y >= getHeight()) {
        LOG(warn, "No neighbours found");
    }
    
    // Up:
    if (y > 0) {
        neighbours.insert(std::make_pair(Direction::UP, coordToId(x, y-1)));
    }
    
    // Right:
    if (x < m_width - 1) {
        neighbours.insert(std::make_pair(Direction::RIGHT, coordToId(x+1, y)));
    }
    // Down:
    if (y < m_height - 1) {
        neighbours.insert(std::make_pair(Direction::DOWN, coordToId(x, y+1)));
    }
    // Left:
    if (x > 0) {
        neighbours.insert(std::make_pair(Direction::LEFT, coordToId(x-1, y)));
    }

    return neighbours;
}

template<typename T>
void TwoVector<T>::clear()
{
    m_width = 0;
    m_height = 0;
    m_items.clear();
}

template<typename T>
void TwoVector<T>::setDimensions(int x, int y)
{
    m_width = x;
    m_height = y;
    m_items.resize(x * y);
}

template<typename T>
void TwoVector<T>::setDimensions(int xy)
{
    setDimensions(xy, xy);
}

#endif
