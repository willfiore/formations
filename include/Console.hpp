#ifndef CONSOLE_H
#define CONSOLE_H

#include <deque>
#include "ConsoleItem.hpp"

class Console
{
public:
    std::deque<ConsoleItem>* getHistory();

    char inputText[256];

    void log(ConsoleItemType, bool continuous, const char* fmt, ...);

    void clearHistory();

    void setVisibility(int);
    bool isVisible();

    bool scrollToBottom = true;
    bool focusOnInput   = true;

    const unsigned int maxSize = 500;

private:
    std::deque<ConsoleItem>  history;

    bool visible = false;

    void addToHistory(ConsoleItem);
};

#define LOG(type, ...)\
    CONSOLE.log(type, false, __VA_ARGS__);

#define LOGC(type, ...)\
    CONSOLE.log(type, true, __VA_ARGS__);

extern Console CONSOLE;

#endif
