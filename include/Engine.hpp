#ifndef ENGINE_H
#define ENGINE_H

#include "EventManager.hpp"
#include "CommandSystem.hpp"
#include "CvarSystem.hpp"
#include "FileSystem.hpp"
#include "InputSystem.hpp"
#include "NetworkSystem.hpp"
#include "RenderSystem.hpp"
#include "Pathfinder.hpp"
#include "Random.hpp"
#include "GameState.hpp"
#include "World.hpp"

class Engine : public EventListener
{
public:
    Engine();
    void init();
    void deinit();

    virtual void onEvent(const Event&) override;
    
    void run();
    
private:
    bool m_running;
    sf::Time m_currentTime;
    
    sf::RenderWindow renderWindow;
    
    EventManager  eventManager;
    
    // Systems
    CommandSystem commandSystem;
    CvarSystem    cvarSystem;
    FileSystem    fileSystem;
    NetworkSystem networkSystem;
    InputSystem   inputSystem;
    RenderSystem  renderSystem;
    Pathfinder    pathfinder;
    Random random;

    // Data
    GameState state;
    World     world;
    
    void    handleInput();
    void    update();

    //
    void CMD_QUIT_F(const Command*, const ArgList&);
    void CMD_LAUNCH_F(const Command*, const ArgList&);

    // EV
    void EV_HOSTED_GAME_F();
    void EV_CONNECTED_TO_GAME_F();
    void EV_DISCONNECTED_FROM_GAME_F();
    void EV_LAUNCH_F();
    void EV_NEXT_TICK_F(const Event&);
    void EV_SELECTION_BOX_F(const Event&);
};

#endif
