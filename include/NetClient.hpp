#ifndef NETCLIENT_H
#define NETCLIENT_H

#include <string>
#include <SFML/Network/IpAddress.hpp>
#include "Player.hpp"

struct NetClient
{
    uint8_t        clid;
    std::string    nickname;
    sf::IpAddress  ip;
    unsigned short port;
    sf::Time       latency;
    uint16_t       lastACK  = 65535;
    uint32_t       lastTick = 0;

    Player*        player;

    bool operator==(const NetClient& b) {
        return clid == b.clid;
    }
};

#endif
