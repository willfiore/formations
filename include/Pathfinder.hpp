#ifndef PATHFINDER_H
#define PATHFINDER_H

#include "IntegrationField.hpp"
#include "TwoVector.hpp"
#include "Sector.hpp"
#include "Portal.hpp"
#include "EventListener.hpp"
#include "World.hpp"

#include <memory>
#include <vector>

class Pathfinder : public EventListener
{
public:
    Pathfinder();
    void inject(World* _w) {
        world = _w;
    }
    void init();
    virtual void onEvent(const Event& e) override;
    void update();
    
    void initNewTileMap();
private:
    static constexpr int SECTOR_SIZE = 10;
    TwoVector<LocalTileID> LOCAL_TILES;

    TileID localToGlobalTile(SectorID sector, LocalTileID tile);

    void createPortals();
    void createPortals(SectorID);
    void formInnerPortalConnections(SectorID);
    void formEdgePortalConnections(SectorID);
    
    void generateCostField();
    void generateCostField(SectorID sector);
    
    IntegrationField* generateIntegrationField(SectorID sector, LocalTileID goal);

    TwoVector<Sector> m_sectors;
    std::vector<std::unique_ptr<Portal> > m_portals;
    
    TwoVector<uint8_t> m_costField;
    std::vector<std::unique_ptr<IntegrationField> > m_integrationFields;

    PortalID nextFreePortalID;

    World* world;
    TwoVector<Tile>* tileMap;
};

#endif
