#ifndef CONSOLEITEM_H
#define CONSOLEITEM_H

#include <string>

enum ConsoleItemType
{
    info,
    warn,
    debug,

    input,
    white,
    red,
    green,
    purple,
    cyan,
    grey
};

struct ConsoleItem
{
    ConsoleItemType type;
    bool            continuous;
    std::string     msg;
};

#endif
