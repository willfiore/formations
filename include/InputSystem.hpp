#ifndef INPUTSYSTEM_H
#define INPUTSYSTEM_H

#include "EventListener.hpp"

#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/Rect.hpp>

#include <string>
#include <boost/bimap.hpp>
#include <map>

class CommandSystem;
class Command;
class ArgList;

class InputSystem : public EventListener
{
public:
    InputSystem();
    void inject(CommandSystem* _c) {
        commandSystem = _c;
    }
    void init();
    virtual void onEvent(const Event& e) override;
    
    void handleInput(sf::Event& e);
    
private:

    void addKeyBinding   (sf::Keyboard::Key, const std::string&);
    void addMouseBinding (sf::Mouse::Button, const std::string&);

    typedef boost::bimap<std::string, sf::Keyboard::Key> string_keymap;
    typedef boost::bimap<std::string, sf::Mouse::Button> string_mousemap;
    string_keymap   m_stringToKey;
    string_mousemap m_stringToMouseButton;
    
    std::map<sf::Keyboard::Key, std::string> m_keyBindings;
    std::map<sf::Mouse::Button, std::string> m_mouseBindings;

    // CMDs
    void CMD_BIND_F(const Command*, const ArgList&);
    void CMD_UNBIND_F(const Command*, const ArgList&);

    CommandSystem* commandSystem;
};

#endif
