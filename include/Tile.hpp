#ifndef TILE_H
#define TILE_H

#include <inttypes.h>

typedef int TileID;
typedef TileID LocalTileID;

struct Tile
{
    TileID id;
    uint16_t debug_integrationCost{0};
    int debug_Color{0};
    bool isWalkable {true};
};

#endif
