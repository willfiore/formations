#ifndef INTEGRATIONFIELD_H
#define INTEGRATIONFIELD_H

#include "TwoVector.hpp"
#include "Sector.hpp"

#include <inttypes.h>

struct IntegratorData
{
    uint16_t cost{0};
    int parent;
};

struct IntegrationField
{
    SectorID    sector;
    LocalTileID goal;
    TwoVector<IntegratorData> field;
};

#endif
