#ifndef RENDERSYSTEM_H
#define RENDERSYSTEM_H

#include "EventListener.hpp"

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/Font.hpp>

class Command;
class ArgList;
class CvarSystem;
class CommandSystem;
class GameState;
class World;
class Pathfinder;

class RenderSystem : public EventListener
{
public:
    RenderSystem();
    void inject(sf::RenderWindow* _r, GameState* _g, World* _w, CvarSystem* _cv, CommandSystem* _co) {
        renderWindow = _r;
        gameState = _g;
        world = _w;
        cvarSystem = _cv;
        commandSystem = _co;
    }

    void init();

    virtual void onEvent(const Event& e) override;

    void update(const sf::Time& currentTime, const sf::Time& delta);

private:
    void drawTiles();
    void drawUnits();
    void drawSelectionBox();
    void drawDebug();
    void drawConsole();

    // CMDs
    void CMD_SELECT_F(const Command*, const ArgList&, bool);

    void EV_WINDOW_RESIZED_F (const Event& e);
    void EV_CVAR_CHANGED_F   (const Event& e);

    float m_lerpValue;
    
    bool m_drawFps;
    sf::Font m_fpsFont;

    bool m_isDrawingSelectionBox;
    sf::IntRect m_selectionBox;
    

    sf::RenderWindow*       renderWindow;

    GameState*  gameState;
    World*      world;
    CvarSystem* cvarSystem;
    CommandSystem* commandSystem;
};

#endif
