#ifndef WORLD_H
#define WORLD_H

#include "TwoVector.hpp"
#include "Player.hpp"
#include "Unit.hpp"
#include "Tile.hpp"

#include <vector>
#include <SFML/System/Time.hpp>

class Engine;

class World
{
public:
    World();
    friend Engine;
    
    Player* addPlayer();
    void    removePlayer(Player*);

    void createEmptyMap(int width, int height);
    void createTestMap(int width, int height);

    std::vector<Unit> const&  getUnitList() const { return m_unitList; }
    std::vector<Player> const& getPlayerList() const { return m_playerList; }
    TwoVector<Tile>& getTileMap() { return m_tileMap; }

private:
    //
    std::vector<Unit> m_unitList;
    Unit* createUnit(sf::Vector2i position);
    void  removeUnit(Unit*);

    TwoVector<Tile> m_tileMap;
    
    std::vector<Player> m_playerList;
};

#endif
