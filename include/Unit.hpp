#ifndef UNIT_H
#define UNIT_H

#include "GameObject.hpp"

#include <SFML/System/Vector2.hpp>
#include <inttypes.h>

struct Unit : public GameObject
{
    uint16_t     id;
    uint8_t      owner;
    bool         selected;

    bool operator==(const Unit& b) {
        return id == b.id;
    }
};

#endif
