#ifndef PLAYER_H
#define PLAYER_H

#include <inttypes.h>

struct Player
{
    uint8_t id;

    bool operator==(const Player& b) {
        return id == b.id;
    }
};

#endif
