#ifndef NETWORKSYSTEM_H
#define NETWORKSYSTEM_H

#include "EventListener.hpp"
#include "NetClient.hpp"
#include "NetMessage.hpp"

#include <SFML/Network.hpp>

#include <memory>
#include <vector>

class Command;
class ArgList;
class GameState;
class Random;
class CvarSystem;
class CommandSystem;
class World;

class NetworkSystem : public EventListener
{
public:
    // Some special values used by NetworkSystem:
    enum MessageType {
        NET_CL_CONNECT,
        NET_SV_CONNECTION_ACCEPTED,
        NET_SV_CLIENT_CONNECTED,
        
        NET_SAY,
        NET_CHANGE_NICKNAME,
        NET_DISCONNECT,
        NET_LAUNCH_GAME,

        NET_TICK_DONE,

        NET_SV_PING,
        NET_CL_PONG,
        
        NET_SV_RELAY,
        NET_ACK
    };
    
    NetworkSystem();
    ~NetworkSystem();
    void inject(GameState* _g, World* _w, Random* _r, CvarSystem* _cv, CommandSystem* _co) {
        gameState = _g;
        world = _w;
        random = _r;
        cvarSystem = _cv;
        commandSystem = _co;
    }
    void init();

    virtual void onEvent(const Event& e) override;
    
    void update(sf::Time currentTime, sf::Time delta);
    
private:
    void setPort(unsigned short);

    // Low Level
    sf::UdpSocket socket;
    std::vector<NetMessage> m_messagesToSend;
    sf::Packet& newMessage(const MessageType);
    sf::Packet& newMessage(NetClient* client, const MessageType);

    void onReceive(sf::Packet& packet, sf::IpAddress& address, unsigned short& port);

    //
    bool m_isServer;
    bool m_isConnected;
    
    std::vector<std::unique_ptr<NetClient>> m_clientList;
    NetClient* addClient();
    void       removeClient(const NetClient*);
    NetClient* getClient(short clid);
    
    std::string formatNickname(NetClient*, std::string);

    sf::Time m_lastUpdateTime;
    sf::Time m_lastResendTime;
    sf::Time m_lastPingTime;

    sf::Time m_fakeLatency;
    std::vector<NetReceivedMessage> m_bufferedReceivedMessages;
    
    NetClient* selfClient;
    NetClient* hostClient;
    NetClient* broadcastClient;
    NetClient* nullClient;
    
    void connect(const sf::IpAddress&, unsigned short port);
    void disconnect();

    void sendTickDone(uint32_t);

    // EVs
    void EV_CVAR_CHANGED_F (const Event& e);
    void EV_CMD_LAUNCH_F   (const Event& e);

    // CMDs
    void CMD_HOST_F       (const Command*, const ArgList&);
    void CMD_CONNECT_F    (const Command*, const ArgList&);
    void CMD_DISCONNECT_F (const Command*, const ArgList&);
    void CMD_CLIENTLIST_F (const Command*, const ArgList&);
    void CMD_SAY_F        (const Command*, const ArgList&);
    
    void RECV_CL_CONNECT_F (sf::Packet&, const sf::IpAddress&, unsigned short port);
    void RECV_SV_CONNECTION_ACCEPTED_F (sf::Packet&, const sf::IpAddress&, unsigned short port);
    void RECV_SV_CLIENT_CONNECTED_F (sf::Packet&);

    void RECV_SV_PING_F (sf::Packet&);
    void RECV_CL_PONG_F (NetClient*, sf::Packet&);

    void RECV_SAY_F             (const NetClient*, sf::Packet&);
    void RECV_CHANGE_NICKNAME_F (      NetClient*, sf::Packet&);
    void RECV_DISCONNECT_F      (const NetClient*, sf::Packet&);
    void RECV_LAUNCH_GAME_F     (const NetClient*, sf::Packet&);
    void RECV_TICK_DONE_F       (      NetClient*, sf::Packet&);

    GameState*  gameState;
    World*      world;
    Random*     random;
    CvarSystem* cvarSystem;
    CommandSystem* commandSystem;
};

#endif
