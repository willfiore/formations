#ifndef COMMAND_H
#define COMMAND_H

#include "Event.hpp"
#include <functional>

enum CommandFlags
{
    CMD_FLAG_NONE      = 1 << 0,
    CMD_FLAG_CONNECTED = 1 << 1,
    CMD_FLAG_INLOBBY   = 1 << 2,
    CMD_FLAG_INGAME    = 1 << 3
};

class ArgList
{
public:
    ArgList() {};
    ArgList(const std::vector<std::string>& argv) { args = argv; }
    ArgList(const std::string& arg) { args.push_back(arg); }

    std::vector<std::string>::size_type count() const { return args.size(); }
    std::string at(const unsigned int m) const { return (m < count()) ? args[m] : ""; }

    void append(const std::string& arg) { args.push_back(arg); };
    void append(const std::vector<std::string>& argv) { args.insert(args.end(), argv.begin(), argv.end()); }
private:
    std::vector<std::string> args;
};

class Command
{
public:
    Command(const std::string& name, const std::function<void(const Command* cmd, const ArgList& args)> func, int flags, const std::string& description, const std::string& usage);

    std::string getName()        const { return name; }
    std::string getDescription() const { return description; }
    std::string getUsage()       const { return usage; }
    int         getFlags()       const { return flags;}

    void execute(const ArgList&) const;

private:
    std::string name;
    std::string description;
    std::string usage;
    int         flags;

    std::function<void(const Command*, const ArgList&)> func;
};

#endif
